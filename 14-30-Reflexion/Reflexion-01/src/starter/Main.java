package starter;

import business.entity.Personne;

public class Main {

	public static void main(String[] args) {

		//////////////////////////////////////////////////////////////////////////////////////
		//(01.)EFFECTUER L'OPERATION DE CREATION SUIVANTE :
		//     ->OBJET A CREER      : UN OBJET DE TYPE 'META-CLASSE DE LA CLASSE Personne'
		//     ->ELEMENT A UTILISER : LE NOM PLEINEMENT QUALIFIE DE LA CLASSE 'Personne'.
		//////////////////////////////////////////////////////////////////////////////////////
		Class<?> metaClassPersonne = null;
		try {
			metaClassPersonne = Class.forName("business.entity.Personne");
			
		} catch (ClassNotFoundException e) {
			System.out.println("Cr�ation d'une m�ta-classe � partir d'un nom de classe � La classe est introuvable");		}
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(02.)EFFECTUER L'OPERATION DE CREATION SUIVANTE :
		//     -->OBJET A CREER      : UN OBJET DE TYPE 'Personne' 
		//     -->ELEMENT A UTILISER : UN OBJET DE TYPE 'META-CLASSE DE LA CLASSE Personne' 
		//////////////////////////////////////////////////////////////////////////////////////
		Personne personne = null;
		try {
			personne = (Personne) metaClassPersonne.newInstance();
			
		} catch (InstantiationException e) {
			System.out.println("Instanciation d'un objet � partir d'une m�ta-classe � La classe n'est pas instanciable");
			
		} catch (IllegalAccessException e) {
			System.out.println("Instanciation d'un objet � partir d'une m�ta-classe � La classe est inaccessible");
		}
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(03.)AFFICHER LE TYPE DE L'OBJET CREE
		//////////////////////////////////////////////////////////////////////////////////////
		System.out.println("+--------------------------------------------+");
		System.out.println("| TYPE DE L'OBJET 'personne' CREE :          |");
		System.out.println("+--------------------------------------------+");
		System.out.println("| TYPE 'Personne' ?                          |");
		System.out.println("+--------------------------------------------+");
		System.out.println("| " + (personne instanceof Personne));
		System.out.println("+--------------------------------------------+");
	}
}
