package starter;

import business.entity.Chat;
import business.entity.Chien;
import business.entity.Personne;

public class Main {

	public static void main(String[] args) {
		

		////////////////////////////////////////////////////////////////////
		//(01.)CREER DES OBJETS DES TYPES SUIVANTS :
		//     -->TYPE 'Personne'
		//     -->TYPE 'Chien'
		//     -->TYPE 'Chat'
		////////////////////////////////////////////////////////////////////
		Personne personneObject = new Personne(); 
		Chien chienObject = new Chien(); 
		Chat chatObject = new Chat(); 
		
		////////////////////////////////////////////////////////////////////
		//(02.)RECUPERER LES OBJETS SUIVANTS :
		//     -->LES OBJETS DE TYPE 'META-CLASSE' CORRESPONDANT AUX OBJETS CREES 
		////////////////////////////////////////////////////////////////////
		Class<?> personneClass = personneObject.getClass();
		Class<?> chienClass = chienObject.getClass();
		Class<?> chatClass = chatObject.getClass();
		
		////////////////////////////////////////////////////////////////////
		//(03.)RECUPERER LES NOMS DES OBJETS SUIVANTS :
		//     -->LES OBJETS DE TYPE 'META-CLASSE' RECUPERES PRECEDEMMENT
		////////////////////////////////////////////////////////////////////
		String personneClassName = personneClass.getName();
		String chienClassName = chienClass.getName();
		String chatClassName = chatClass.getName();
		
		////////////////////////////////////////////////////////////////////
		//(04.)AFFICHER LES TYPES DES OBJETS CREES PRECEDEMMENT
		////////////////////////////////////////////////////////////////////
		System.out.println("+--------------------------------------------+");
		System.out.println("| TYPE DE L'OBJET 'personneObject' :         |");
		System.out.println("+--------------------------------------------+");
		System.out.println("| TYPE 'Personne' ?                          |");
		System.out.println("+--------------------------------------------+");
		System.out.println("| " + (personneObject instanceof Personne));
		System.out.println("+--------------------------------------------+");

		System.out.println("+--------------------------------------------+");
		System.out.println("| TYPE DE L'OBJET 'chienObject' :            |");
		System.out.println("+--------------------------------------------+");
		System.out.println("| TYPE 'Chien' ?                             |");
		System.out.println("+--------------------------------------------+");
		System.out.println("| " + (chienObject instanceof Chien));
		System.out.println("+--------------------------------------------+");

		System.out.println("+--------------------------------------------+");
		System.out.println("| TYPE DE L'OBJET 'chatObject' :             |");
		System.out.println("+--------------------------------------------+");
		System.out.println("| TYPE 'Chat' ?                              |");
		System.out.println("+--------------------------------------------+");
		System.out.println("| " + (chatObject instanceof Chat));
		System.out.println("+--------------------------------------------+");

		////////////////////////////////////////////////////////////////////
		//(05.)AFFICHER LE NOM DE L'OBJET DE TYPE 'META-CLASSE'
		////////////////////////////////////////////////////////////////////
		System.out.println("+--------------------------------------------+");
		System.out.println("| META-CLASSE DES OBJETS :                   |");
		System.out.println("+--------------------------------------------+");
		System.out.println("| 'Personne' : " + personneClassName + "     |");
		System.out.println("| 'Chien'    : " + chienClassName    + "     |");
		System.out.println("| 'Chat'     : " + chatClassName     + "     |");
		System.out.println("+--------------------------------------------+");
	}
}
