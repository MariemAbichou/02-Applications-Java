package Model;

public class Vehicule {
	
	
	private String marque;
	private String puissance;
	private int kilometrageParcouru;
	private static Vehicule instance; //je declare ma variable static "en mode priv�" instance
	
	
// constructeur en mode prive .... c'est oblig� et c'est important
	private Vehicule() {} ;
	
// singleton
		public static Vehicule getInstance() {
		
		if (Vehicule.instance == null) {
			Vehicule v = new Vehicule(); // l'objet "v" va etre d�truite apres l'accolade fermante il faut la sotcker, stocker la reference vite dans une variable static
			Vehicule.instance = v ;  // ici le "=" affecte la ref�rence de l'objet v mais sera stocker dans memoire static donc plus durable
			System.out.println("Nous venons d'instancier une nouvelle vehicule !!!");
			
		} else {
			System.out.println("Nous n'avons rien instanci� du tout. L'objet existe d�j� !!!");;
		} return Vehicule.instance;
	    }
// getteur et setteur
		public String getMarque            () { return this.marque;    }
		public String getPuissance         () { return this.puissance; }
		public int getKilometrageParcouru  () { return this.kilometrageParcouru;    }
		
		public void setMarque   (String pMarque   ) { this.marque    = pMarque;    }
		public void setPuissance(String pPuissance) { this.puissance = pPuissance; }
		public void setKilometrageParcouru   (int pKilometrageParcouru) { this.kilometrageParcouru = pKilometrageParcouru;}	

		// methode pour afficher 
		public void editer() {
			System.out.println("");
			System.out.println("*****************************************************");
			System.out.println("*******l'instance de l'objet est:"+ Vehicule.instance);
			System.out.println("*****************************************************");
			System.out.println("**********AFFICHAGE D'INFO VEHICULE **********");				
			System.out.println("|Marque     |" + marque+"|");
			System.out.println("|Puissance  |" + puissance+"|");
			System.out.println("|Km         |" + kilometrageParcouru+"|");

	    }

}

