package gestionVehiculePackage;

public class Vehicules {
	private String marque;
	private int puissance;
	
	
	
	//Constructeur
	
	public Vehicules (String vmarque, int vpuissance) {
		
		this.marque = vmarque;
		this.puissance = vpuissance;
	}
	public void afficher() {
		
		System.out.println("************Afficher les vehicules*************");
		System.out.println("Marque :" + marque);
		System.out.println("Puissance :" + puissance);
	}
	
	public String getMarque() {
		return marque;
	}
	public int getPuissance() {
		return puissance;
	}
	
	public void  setMarque(String marque) {
		this.marque = marque;
	}
	public void setPuissance(int puissance) {
		this.puissance = puissance;
	}

}
