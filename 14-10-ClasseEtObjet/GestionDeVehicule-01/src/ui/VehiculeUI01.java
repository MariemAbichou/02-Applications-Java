package ui;

import java.util.Scanner;

import Entites.Vehicule01;


public class VehiculeUI01 {
	
		public void sequencePrincipale()
		{
		Vehicule01 vSaisie = this.saisir();
		this.editer(vSaisie);
		}
	/////////
		public Vehicule01 saisir() 
		{
		
			System.out.println("+------------------------------------------------------------+");
			System.out.println("| VEUILLEZ SAISIR LES INFORMATIONS RELATIVES A LA VEHICULE : |");
			System.out.println("+------------------------------------------------------------+");
	
			Scanner scanner = new Scanner(System.in);
	
			System.out.print("| Marque : ");
			String marque = scanner.nextLine();
			
			System.out.print("| Puissance : ");
			String puissance = scanner.nextLine();
	
			System.out.print("| Kilometrage Parcouru : ");
			int kilometrageParcouru = scanner.nextInt();
	
			Vehicule01 vSaisie = new Vehicule01(marque, puissance, kilometrageParcouru);
			
			scanner.close();
			return vSaisie;
		} 
		//////
		
	
	public void editer(Vehicule01 vVehicule) {
		
		System.out.println("+------------------------------------------------------------+");
		System.out.println("| LES INFORMATIONS RELATIVES A LA PERSONNE SAISIE :          |");
		System.out.println("+----------------+-------------------+-----------------------+");
		System.out.println("| Marque :          | Puissance :          | Kilometrage :   |");
		System.out.println("+----------------+-------------------+-----------------------+");
		System.out.println("|       " + vVehicule.getMarque() + "      |         " + vVehicule.getPuissance() + "         |   " + vVehicule.getKilometrage() + "          |");
		System.out.println("+----------------+-------------------+-----------------------+");
		
		return;
	}

}
