package Entites;

public class Vehicule01 {
	
	private String marque;
	private String puissance;
	private int kilometrageParcouru;
	
	// constructeur de la classe
    public Vehicule01() {}
	
			public Vehicule01 (String vMarque, String vPuissance, int vKilometrageParcouru) {
				
				this.marque = vMarque;
				this.puissance = vPuissance;
				this.kilometrageParcouru = vKilometrageParcouru;
	}

	// methodes de la classe		
	public void editer() {
				System.out.println("**********AFFICHAGE D'INFO VEHICULE********************");
				System.out.println("Marque : " + marque);
				System.out.println("Puissance : " + puissance);
				System.out.println("Kilometrage Parcouru : " + kilometrageParcouru);
				System.out.println("*****************************************************");
			}
	
	///////////////////////////////////////////////////////////////////////////
	// 4. ACCESSEURS EN LECTURE DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	
	public String getMarque()           { return marque; };
	public String getPuissance()        { return puissance; };
	public int    getKilometrage()           { return kilometrageParcouru; };

	///////////////////////////////////////////////////////////////////////////
	// 5. ACCESSEURS EN ECRITURE DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public void setMarque(String marque)               { this.marque    = marque; };
	public void setPuissance(String puissance)             { this.puissance = puissance; };
	public void setKilometrage(int kilometrageParcouru)     { this.kilometrageParcouru = kilometrageParcouru; };
}
