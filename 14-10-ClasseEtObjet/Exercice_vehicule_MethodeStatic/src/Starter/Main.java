package Starter;

import Model.Vehicule;

public class Main {

	public static void main(String[] args) {
		
		Vehicule veh1 = Vehicule.getInstance();
		veh1.setMarque   (" toyota ");
		veh1.setPuissance("  8 cv  ");
		veh1.setKilometrageParcouru(1500);
		veh1.editer();		
		
		Vehicule veh2 = Vehicule.getInstance();		
		veh2.setMarque   ("mercedes");
		veh2.setPuissance("  4 cv  ");
		veh2.setKilometrageParcouru(1044);
		veh2.editer();
		
		Vehicule veh3 = Vehicule.getInstance();	
		veh3.setMarque   (" peugeot");
		veh3.setPuissance("  8 cv  ");
		veh3.setKilometrageParcouru(1020);
		veh3.editer();
		


		
	}

}