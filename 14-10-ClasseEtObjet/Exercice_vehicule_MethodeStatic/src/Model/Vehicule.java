package Model;

public class Vehicule {
	
	public static int compteur = 0;  //attribut "static"
	private String marque;
	private String puissance;
	private int kilometrageParcouru;	

    private Vehicule() {}           //Le constructeur est rendu priv� donc pas possible d'y acceder de l'exterieur 
        							//de la classe cad on peut plus l'appeler dans Starter-->MAIN

    //  la methode getInstance est une methode "static"
    //////////////
				public static Vehicule getInstance() {   // on passe par la methode getInstance qui va construire les objets //cette methode est -- statis et public et retourne en sortie un objet de type Classe Vehicule--
					Vehicule v = new Vehicule();       // on instancie l'objet v de la classe v --> cette methode nous retourne un objet v accessible par main
					compteur++;                       // � chaque fois on contruit un nouveau Objet Vehicule, le compteur va �tre incrementer pour contenir le nombre d'objet creer
					return v;
				}
	
				public String getMarque            () { return this.marque;    }
				public String getPuissance         () { return this.puissance; }
				public int getKilometrageParcouru  () { return this.kilometrageParcouru;    }
				
				public void setMarque   (String pMarque   ) { this.marque    = pMarque;    }
				public void setPuissance(String pPuissance) { this.puissance = pPuissance; }
				public void setKilometrageParcouru   (int pKilometrageParcouru) { this.kilometrageParcouru = pKilometrageParcouru;}	
	
				public void editer() {
					System.out.println("");
					System.out.println("**********AFFICHAGE D'INFO VEHICULE N�:" + Vehicule.compteur+ "   **********");				
					System.out.println("|Marque     |" + marque+"|");
					System.out.println("|Puissance  |" + puissance+"|");
					System.out.println("|Km         |" + kilometrageParcouru+"|");
					System.out.println("*****************************************************");
			    }
}

////Note
/// un autre constructeur avec une autre signature ... SURCHARGE::::non utilis dans cette exemple////
			//public Vehicule (String vMarque, String vPuissance, int vKilometrageParcouru) {
			//Vehicule.compteur++;
			//this.marque = vMarque;
			//this.puissance = vPuissance;
			//this.kilometrageParcouru = vKilometrageParcouru;					
			//}