package domain;

public class Personne {

	
	///////////////////////////////////////////////////////////////////////////
	// 1. ATTRIBUTS DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	private String nom;
	private String prenom;
	private int age;
	
	///////////////////////////////////////////////////////////////////////////
	// 2. CONSTRUCTEURS DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	
	public Personne() {} // C'est un constructeur qui existe par defaut , �a va nous servir plus tard "Java entreprise edition" ne prend pas d'argument

	public Personne(String pNom, String pPrenom, int pAge) {   //pNom, pPrenom, pAge --> sotoker dans Stack memory
		
		this.nom = pNom;
		this.prenom = pPrenom;
		this.age = pAge;
	}	
	///////////////////////////////////////////////////////////////////////////
	// 3. METHODES DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public void afficher() {
		System.out.println("**********AFFICHAGE D'UNE PERSONNE********************");
		System.out.println("Nom : " + nom);
		System.out.println("Pr�nom : " + prenom);
		System.out.println("Age : " + age);
		System.out.println("*****************************************************");
	}
	
	///////////////////////////////////////////////////////////////////////////
	// 4. ACCESSEURS EN LECTURE DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public String getNom()           { return nom; }
	public String getPrenom()        { return prenom; }
	public int    getAge()           { return age; }
	
	///////////////////////////////////////////////////////////////////////////
	// 5. ACCESSEURS EN ECRITURE DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public void setNom(String nom)       { this.nom    = nom; }
	public void setPrenom(String prenom) { this.prenom = prenom; }
	public void setAge(int age)          { this.age    = age; }
}
