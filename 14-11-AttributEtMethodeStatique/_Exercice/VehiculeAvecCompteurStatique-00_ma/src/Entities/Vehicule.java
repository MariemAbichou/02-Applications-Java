package Entities;

public class Vehicule {
	
	public static int compteur = 0;
	private String marque;
	private String puissance;
	private int kilometrageParcouru;	

    private Vehicule() {}
    
//			public Vehicule (String vMarque, String vPuissance, int vKilometrageParcouru) {
//				Vehicule.compteur++;
//				this.marque = vMarque;
//				this.puissance = vPuissance;
//				this.kilometrageParcouru = vKilometrageParcouru;					
//			}
			
	public static Vehicule getInstance() {   // on passe par la methode getInstance qui va construire les objets
		
		Vehicule v = new Vehicule();    // on instancie l'objet p de la classe p --> cette methode nous retourne un objet p accessible par main
		compteur++;
		return v;
	}
	
			public String getMarque            () { return this.marque;    }
			public String getPuissance         () { return this.puissance; }
			public int getKilometrageParcouru  () { return this.kilometrageParcouru;    }
			
			public void setMarque   (String pMarque   ) { this.marque    = pMarque;    }
			public void setPuissance(String pPuissance) { this.puissance = pPuissance; }
			public void setKilometrageParcouru   (int pKilometrageParcouru) { this.kilometrageParcouru = pKilometrageParcouru;}	
	
				public void editer() {
				System.out.println("**********AFFICHAGE D'INFO VEHICULE********************");				
				System.out.println("COMPTEUR : " + Vehicule.compteur);
				System.out.println("Marque : " + marque);
				System.out.println("Puissance : " + puissance);
				System.out.println("Kilometrage Parcouru : " + kilometrageParcouru);
				System.out.println("*****************************************************");
			  }
}
