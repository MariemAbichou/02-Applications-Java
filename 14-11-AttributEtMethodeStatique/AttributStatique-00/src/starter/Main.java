package starter;
import domain.Personne;

public class Main {

	public static void main(String[] args) {

		
		///////////////////////////////////////////////////////////////////////
		// CREER DES OBJETS DU TYPE 'PERSONNE'
		///////////////////////////////////////////////////////////////////////
		//TODO CREATION DE PERSONNES !!!
		
		System.out.println("Compteur :" + Personne.compteur);
		
		Personne personne1 = new Personne("Dalgalian", "Tcharou" , 49); // ON DIT QUE personne1 c'est la "r�f�rence" de l'objet Classe
		personne1.afficher();
		
		Personne personne2 = new Personne("Pagan", "Jean-Jacques" , 44);
		personne2.afficher();
		
		Personne personne3 = new Personne("Bachri", "Amin" , 42);
		personne3.afficher();
		
	}

}
