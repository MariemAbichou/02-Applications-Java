package domain;

public class Personne {

	
	///////////////////////////////////////////////////////////////////////////
	// 1. ATTRIBUTS DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	private static int compteur;
	private String nom;
	private String prenom;
	private int age;
	
	///////////////////////////////////////////////////////////////////////////
	// 2. CONSTRUCTEURS DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	
	private Personne() {   // ICI LE constructeur est priv� donc non acc�cible dans main
		
		Personne.compteur++;
	}

	///////////////////////////////////////////////////////////////////////////
	// 3. METHODES DE CLASSE 
	///////////////////////////////////////////////////////////////////////////
	
	public static Personne getInstance() {   // on passe par la methode getInstance qui va construire les objets
		
		Personne p = new Personne();    // on instancie l'objet p de la classe p --> cette methode nous retourne un objet p accessible par main
		return p;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// 4. METHODES D'INSTANCE
	///////////////////////////////////////////////////////////////////////////
	public void afficher() {
		System.out.println("**********AFFICHAGE D'UNE PERSONNE********************");
		System.out.println("Compteur : " + Personne.compteur);
		System.out.println("Nom      : " + this.nom         );
		System.out.println("Pr�nom   : " + this.prenom      );
		System.out.println("Age      : " + this.age         );
		System.out.println("*****************************************************");
	}

	public String getNom    ()        { return this.nom;    }
	public String getPrenom ()        { return this.prenom; }
	public int    getAge    ()        { return this.age;    }
	
	public void setNom   (String pNom   ) { this.nom    = pNom;    }
	public void setPrenom(String pPrenom) { this.prenom = pPrenom; }
	public void setAge   (int    pAge   ) { this.age    = pAge;    }
}
