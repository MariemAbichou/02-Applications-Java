/**
 * 
 */
package factory;

import model.Animal;

/**
 * @author 1603599
 *
 */
public class AnimalFactory {

	private String objectType;
	
	public void configure(String pType) {
		this.objectType = pType;
	}
	
	public Animal getObject() {
		
		Animal animal = null;
		
		// INSTANCIER UN OBJET CONNAISSANT LE NOM DE SA CLASSE
		try {
			animal = (Animal)Class.forName(this.objectType).newInstance();
			
		} catch (InstantiationException e) {
			
			System.out.println("Tentative de cr�ation d'un objet : La classe demand�e n'est pas instanciable !!!");
			
		} catch (IllegalAccessException e) {
			
			System.out.println("Tentative de cr�ation d'un objet : La classe demand�e n'est pas accessible !!!");
			
		} catch (ClassNotFoundException e) {
			
			System.out.println("Tentative de cr�ation d'un objet : La classe demand�e n'a pas �t� trouv�e !!!");
		}
		
		return animal;
	}
}
