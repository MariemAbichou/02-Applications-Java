/**
 * 
 */
package main;

import ui.UserInterface;

/**
 * @author 1603599
 *
 */
public class Main{

	/**
	 * <b>CONSTRUCTEUR</b>
	 */
	public Main() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * <b>CONSTRUCTEUR</b>
	 * @param args
	 */
	public static void main(String[] args) {
		
		///////////////////////////////////////////////////////////////////////
		// 01.CREER UNE USER-INTERFACE
		///////////////////////////////////////////////////////////////////////
		UserInterface userInterface = new UserInterface();
		
		///////////////////////////////////////////////////////////////////////
		// 02.APPELER LA METHODE 'EXECUTE' DE LA USER-INTERFACE
		///////////////////////////////////////////////////////////////////////
		userInterface.execute();
	}

}
