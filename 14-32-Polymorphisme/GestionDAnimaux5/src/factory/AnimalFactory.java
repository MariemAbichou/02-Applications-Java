/**
 * 
 */
package factory;



import java.util.HashMap;
import java.util.Map;

import model.Animal;

/**
 * @author 1603599
 *
 */
public class AnimalFactory {

	
	/**
	 * <b>TABLE DES TYPES D'ANIMAUX</b>
	 */
	private Map<String, String> animalTypes;
	/**
	 * <b>TYPE D'ANIMAL SELECTIONNE</b>
	 */
	private String objectType;
	

	
	/**
	 * <b>CONSTRUCTEUR</b>
	 */
	public AnimalFactory () {
		
		this.animalTypes = new HashMap<String, String>();
		
		animalTypes.put("Chat", "model.mammifere.Chat");
		animalTypes.put("Chien", "model.mammifere.Chien");
		animalTypes.put("Grenouille", "model.amphibien.Grenouille");
		animalTypes.put("Crocodile", "model.amphibien.Crocodile");
	}
	
	
	/**
	 * <b>VERIFIER LE NOM D'ANIMAL FOURNI</b>
	 */
	public boolean checkName(String pName){
		
		// VERIFIER LA PRESENCE, DANS LA MAP, DE LA CLE FOURNIE
		boolean exists = this.animalTypes.containsKey(pName);
		
		return exists;
	}
	
	/**
	 * <b>VERIFIER LA PRESENCE DE DANS L'ANIMAL-FACTORY DE LA CLE DEMANDEE</b>
	 * <b>CONFIGURER L'ANIMAL-FACTORY</b>
	 */
	public void configure(String pType) {
		
		boolean exists = this.checkName(pType);
		
		if (exists) {
			
			String animalFullyQualifiedStr = animalTypes.get(pType);
			this.objectType = animalFullyQualifiedStr;
		} 
	}
	
	/**
	 * <b>CONFIGURER L'ANIMAL-FACTORY</b>
	 */
	@SuppressWarnings("unchecked")
	public Animal getObject() {
		
		Animal animal = null;
		
		///////////////////////////////////////////////////////////////////////
		// PROCEDE DE REFLEXION :
		//   ->INSTANCIER UN OBJET CONNAISSANT LE NOM DE SA CLASSE.
		///////////////////////////////////////////////////////////////////////
		try {
			//CHARGER LA CLASSE 'Animal' EN MEMOIRE (EN ZONE STATIQUE)
			Class<Animal> animalClass = (Class<Animal>)Class.forName(this.objectType);
			
			System.out.println("Chargement de la classe en memoire : " + animalClass.getClass().getName());
			
			//INSTANCIER UN OBJET DE LA CLASSE 'Animal' (DANS LE TAS)
			animal = animalClass.newInstance();
			
		} catch (InstantiationException e) {
			
			System.out.println("Tentative de cr�ation d'un objet : La classe demand�e n'est pas instanciable !!!");
			
		} catch (IllegalAccessException e) {
			
			System.out.println("Tentative de cr�ation d'un objet : La classe demand�e n'est pas accessible !!!");
			
		} catch (ClassNotFoundException e) {
			
			System.out.println("Tentative de cr�ation d'un objet : La classe demand�e n'a pas �t� trouv�e !!!");
			
		} catch (NullPointerException e) {
			
			System.out.println("Tentative de cr�ation d'un objet : Op�ration avec un pointeur null !!!");
			
		}
		
		return animal;
	}
}
