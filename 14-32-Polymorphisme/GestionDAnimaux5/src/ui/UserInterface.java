/**
 * 
 */
package ui;

import java.util.Scanner;

import user.AnimalUser;

/**
 * @author 1603599
 *
 */
public class UserInterface {

	public AnimalUser animalUser;
	
	/**
	 * <b>CONTRUCTEUR QUI INSTANCIE LES ATTRIBUTS</b>
	 */
	public UserInterface() {
		
		this.animalUser = new AnimalUser();		
	}
	
	
	/**
	 * <b>EXECUTION DU TRAITEMENT DE L'INTERFACE UTILISATEUR</b>
	 */
	public void execute () {
		
		Scanner sc = new Scanner(System.in);
		String repeatStr;
		
		do {
			
			boolean checkOK = true;
			String animalStr = null;
			do {
				///////////////////////////////////////////////////////////////////////
				// (1.) SAISIR LE NOM DE L'ANIMAL
				///////////////////////////////////////////////////////////////////////
				System.out.println("Veuillez saisir le nom de l'animal souhait�, svp :");
				animalStr = sc.nextLine();
				System.out.println("Vous avez saisi : " + animalStr);
		
				///////////////////////////////////////////////////////////////////////
				// (2.) VERIFIER LE NOM SAISI
				///////////////////////////////////////////////////////////////////////
				checkOK = this.animalUser.checkAnimalName(animalStr);
				
				if (!checkOK) {
					System.out.println("Erreur : L'animal saisi n'existe pas !");
				}
			
			} while(!checkOK); 
			
			///////////////////////////////////////////////////////////////////////
			// (3.) LANCER LE TRAITEMENT DE L'ANIMAL-USER
			///////////////////////////////////////////////////////////////////////
			this.animalUser.execute(animalStr);
			
			///////////////////////////////////////////////////////////////////////
			// (4.) SAISIR L'OPTION DE PROLONGATION
			///////////////////////////////////////////////////////////////////////
			System.out.println("Voulez-vous recommencer (O / N) ?");
			repeatStr = sc.nextLine();
			System.out.println("Vous avez saisi : " + repeatStr);
			
		} while(repeatStr.equals("O"));
		
		sc.close();
		
		System.out.println("Le programme va se terminer...");
	}
}
