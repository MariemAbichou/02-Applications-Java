package model;
/**
 * 
 */

/**
 * @author 1603599
 *
 */
public class Personne implements Comparable<Personne> {

	private String nom;
	private String prenom;
	private int age;
	
	public Personne() {
		
		this.nom = "";
		this.prenom = "";
		this.age = 0;
	}
	
	public Personne(String pNom, String pPrenom, int pAge) {
		
		this.nom = pNom;
		this.prenom = pPrenom;
		this.age = pAge;
	}
	
	/**
	 * <b>COMPARER LA PERSONNE FOURNIE EN PARAMETRE AVEC L'OBJET COURANT (DE TYPE PERSONNE)</b>
	 * 
	 * @param pPersonne L'objet 'Personne' pass� en param�tre
	 * 
	 * @return   int<br/>
	 *            0  si (l'age de l'objet courant) = (l'age de l'objet pass� en param�tre).<br/>
	 *           -1  si (l'age de l'objet courant) < (l'age de l'objet pass� en param�tre).<br/>
	 *           +1  si (l'age de l'objet courant) > (l'age de l'objet pass� en param�tre).
	 */
	@Override
	public int compareTo(Personne pPersonne) {
		
		////////////////////////////////////////////////////////////////////////
		//COMPARER LA PERSONNE FOURNIE EN PARAMETRE AVEC L'OBJET COURANT (DE TYPE PERSONNE)
		////////////////////////////////////////////////////////////////////////
		 if (this.age < pPersonne.age) {
			System.out.println("(l'age de l'objet courant) < (l'age de l'objet pass� en param�tre)");
			return -1;
			
		} else if (this.age > pPersonne.age) {
			System.out.println("(l'age de l'objet courant) > (l'age de l'objet pass� en param�tre)");
			return 1;
			
		} else  {
			System.out.println("(l'age de l'objet courant) = (l'age de l'objet pass� en param�tre)");
			return 0;
		}
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	
}
