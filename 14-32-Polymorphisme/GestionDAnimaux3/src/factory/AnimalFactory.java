/**
 * 
 */
package factory;



import java.util.HashMap;
import java.util.Map;

import model.Animal;

/**
 * @author 1603599
 *
 */
public class AnimalFactory {

	
	/**
	 * <b>TABLE DES TYPES D'ANIMAUX</b>
	 */
	private Map<String, String> animalTypes;
	/**
	 * <b>TYPE D'ANIMAL SELECTIONNE</b>
	 */
	private String objectType;
	

	
	/**
	 * <b>CONSTRUCTEUR</b>
	 */
	public AnimalFactory () {
		
		this.animalTypes = new HashMap<String, String>();
		
		animalTypes.put("Chat", "model.mammifere.Chat");
		animalTypes.put("Chien", "model.mammifere.Chien");
		animalTypes.put("Grenouille", "model.amphibien.Grenouille");
		animalTypes.put("Crocodile", "model.amphibien.Crocodile");
	}
	
	/**
	 * <b>CONFIGURER L'ANIMAL-FACTORY</b>
	 */
	public void configure(String pType) {
		
		String animalFullyQualifiedStr = animalTypes.get(pType);
		
		this.objectType = animalFullyQualifiedStr;
	}
	
	/**
	 * <b>CONFIGURER L'ANIMAL-FACTORY</b>
	 */
	public Animal getObject() {
		
		Animal animal = null;
		
		///////////////////////////////////////////////////////////////////////
		// PROCEDE DE REFLEXION :
		//   ->INSTANCIER UN OBJET CONNAISSANT LE NOM DE SA CLASSE.
		///////////////////////////////////////////////////////////////////////
		try {
			animal = (Animal)Class.forName(this.objectType).newInstance();
			
		} catch (InstantiationException e) {
			
			System.out.println("Tentative de cr�ation d'un objet : La classe demand�e n'est pas instanciable !!!");
			
		} catch (IllegalAccessException e) {
			
			System.out.println("Tentative de cr�ation d'un objet : La classe demand�e n'est pas accessible !!!");
			
		} catch (ClassNotFoundException e) {
			
			System.out.println("Tentative de cr�ation d'un objet : La classe demand�e n'a pas �t� trouv�e !!!");
		}
		
		return animal;
	}
}
