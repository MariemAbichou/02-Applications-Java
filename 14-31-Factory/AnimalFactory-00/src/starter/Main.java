package starter;

import business.entity.Animal;
import business.entity.Chat;
import business.entity.Chien;
import business.factory.AnimalFactory;

public class Main {

	public static void main(String[] args) {

		//////////////////////////////////////////////////////////////////////////////////////
		//(01.)CREER UNE FABRIQUE D'ANIMAUX :
		//     ->TYPE DE L'OBJET : 'AnimalFactory'
		//////////////////////////////////////////////////////////////////////////////////////
		AnimalFactory animalFactory = new AnimalFactory();
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(02.)DEMANDER A LA FABRIQUE D'ANIMAUX D'EFFECTUER L'OPERATION SUIVANTE :
		//     -->OBJET A CREER      : UN OBJET DE TYPE 'Animal' 
		//////////////////////////////////////////////////////////////////////////////////////
		Animal animalCree = animalFactory.create();
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(03.)AFFICHER LE TYPE DE L'OBJET CREE
		//////////////////////////////////////////////////////////////////////////////////////
		System.out.println("+--------------------------------------------+");
		System.out.println("| TYPE DE L'OBJET 'Animal' CREE :            |");
		System.out.println("+----------------+---------------------------+");
		System.out.println("| TYPE 'Chien' : | " + (animalCree instanceof Chien));
		System.out.println("+----------------+---------------------------+");
		System.out.println("| TYPE 'Chat' : | " + (animalCree instanceof Chat));
		System.out.println("+----------------+---------------------------+");
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(04.)TESTER LE COMPORTEMENT DE L'OBJET CREE
		//////////////////////////////////////////////////////////////////////////////////////
		animalCree.crier();
	}
}
