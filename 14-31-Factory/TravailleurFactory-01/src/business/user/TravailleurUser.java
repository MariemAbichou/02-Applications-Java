package business.user;

import business.entity.ITravailleur;
import business.factory.TravailleurFactory;

/**
 * <b>CETTE CLASSE EST UN UTILISATEUR D'OBJETS DU TYPE 'ITravailleur' :</b>
 * @author Tcharou DALGALIAN
 *
 */
public class TravailleurUser {

	
	private TravailleurFactory travailleurFactory;
	
	
	/**
	 * <b>CONSTRUCTEUR AVEC 1 ARGUMENT</b>
	 * @param pTravailleurFactory
	 */
	public TravailleurUser(TravailleurFactory pTravailleurFactory) {
		
		this.travailleurFactory = pTravailleurFactory;
	}
	
	
	/**
	 * <b>EFFECTUER UN TRAITEMENT</b>
	 * @return
	 */
	public void work() {
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(01.)DEMANDER A LA FABRIQUE D'ANIMAUX D'EFFECTUER LA CREATION D'OBJET SUIVANTE :
		//     -->OBJET A CREER      : UN OBJET DE TYPE 'Animal' 
		//////////////////////////////////////////////////////////////////////////////////////
		ITravailleur travailleurCree = this.travailleurFactory.create();
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(02.)DECLENCHER LE COMPORTEMENT 'crier' DE L'OBJET CREE
		//////////////////////////////////////////////////////////////////////////////////////
		travailleurCree.travailler();
	}
}
