package business.factory;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.reflections.Reflections;

import business.entity.ITravailleur;


/**
 * <b>CETTE CLASSE EST UNE FABRIQUE D'OBJETS DU TYPE 'Animal' :</b>
 * @author Tcharou
 *
 */
public class TravailleurFactory {
	
	
	/**
	 * <b>MAP DESTINEE A STOCKER DES COUPLES DECRITS CI-DESSOUS :</b><br>
	 * <br>
	 * -->CLES    : LES TYPES D'OBJETS A FABRIQUER<br>
	 * -->VALEURS : LES FQN (FULLY QUALIFIED NAME) DES TYPE D'OBJETS A FABRIQUER
	 */
	private Map<String, String> travailleurTypes;
	
	/**
	 * <b>CHAINE DE CARACTERES DESTINEE A STOCKER LE TYPE D'OBJET A FABRIQUER</b>
	 */
	private String travailleurType;
	
	
	/**
	 * <b>CONSTRUCTEUR SANS ARGUMENT</b>
	 */
	public TravailleurFactory() {
		this.actualize();
	}
	
	/**
	 * <b>ACTUALISER LA COLLECTION DES TYPES D'OBJETS A CONSTRUIRE</b>
	 */
	private void actualize() {

		///////////////////////////////////////////////////////////////////
		//(01.)CREER UN 'EXPLORATEUR' POUR L'ARBORESCENCE DE PACKAGES SPECIFIEE 
		///////////////////////////////////////////////////////////////////
		Reflections reflections = new Reflections("business.entity");

		///////////////////////////////////////////////////////////////////
		//(02.)IDENTIFIER LES CLASSES HERITANT DU TYPE 'ITravailleur' (POUR L'EXPLORATEUR SPECIFIE).
		///////////////////////////////////////////////////////////////////
	    Set<Class<? extends ITravailleur>> travailleurSubTypes = reflections.getSubTypesOf(ITravailleur.class);
	     
		///////////////////////////////////////////////////////////////////
		//(03.)CREER LA COLLECTION DE TYPES 
		///////////////////////////////////////////////////////////////////
		this.travailleurTypes = new HashMap<String, String>();

		///////////////////////////////////////////////////////////////////
		//(04.)ALIMENTER LA COLLECTION DE TYPES A L'AIDE DU SET D'OBJETS RECUPERES
		///////////////////////////////////////////////////////////////////
	    for (Class<? extends ITravailleur> subType : travailleurSubTypes) {
	    	
	    	String subTypeSimpleName = subType.getSimpleName();
	    	String subTypeFullyQualifedName = subType.getName();
	    	
			this.travailleurTypes.put(subTypeSimpleName, subTypeFullyQualifedName);
			
			System.out.println("Travailleur-Types Simple Names          : " + subTypeSimpleName       );
			System.out.println("Travailleur-Types Fully Qualified Names : " + subTypeFullyQualifedName);
		}
	}
	
	
	
	/**
	 * <b>CONFIGURER LA FABRICATION D'OBJETS</b>
	 * @param pTravailleurType Le type d'objet � fabriquer.
	 */
	public void configure(String pTravailleurType) {
		
		/////////////////////////////////////////////////////////////
		//(00.)EXTRAIRE DE LA MAP L'OBJET SUIVANT :
		//     ->LE FQN (FULLY QUALIFIED NAME) CORRESPONDANT AU TYPE D'OBJET A CREER.
		/////////////////////////////////////////////////////////////
		String travailleurTypeFQN = this.travailleurTypes.get(pTravailleurType);
		
		/////////////////////////////////////////////////////////////
		//(01.)ALIMENTER L'ATTRIBUT 'travailleurType'
		/////////////////////////////////////////////////////////////
		this.travailleurType = travailleurTypeFQN;
	}
	
	/**
	 * <b>CREER L'OBJET DEMANDE</b>
	 * @return L'objet fabriqu�.
	 */
	public ITravailleur create() {
		
		/////////////////////////////////////////////////////////////
		//(01.)CREER LES META-CLASSES SUIVANTES :
		//     -->UNE META-CLASSE DE LA CLASSE "business.entity.ITravailleur"
		/////////////////////////////////////////////////////////////
		Class<?> metaClasseTravailleur = null;
		try {
			metaClasseTravailleur = Class.forName(this.travailleurType);
			
		} catch (ClassNotFoundException e) {
			System.out.println("Cr�ation d'une m�ta-classe � partir de son nom -- La classe est introuvable");
		}
		/////////////////////////////////////////////////////////////
		//(02.)CREER UN OBJET DU TYPE DECRIT CI-DESSOUS :
		//     -->LE TYPE POUR LEQUEL LA FABRIQUE A ETE CONFIGUREE.
		/////////////////////////////////////////////////////////////
		ITravailleur travailleurCree = null;
		try {
			travailleurCree = (ITravailleur) metaClasseTravailleur.newInstance();
			
		} catch (InstantiationException e) {
			System.out.println("Cr�ation d'un objet � partir de sa m�ta-classe -- La classe n'est pas instanciable");
			
		} catch (IllegalAccessException e) {
			System.out.println("Cr�ation d'un objet � partir de sa m�ta-classe -- La classe n'est pas accessible");
		}
		/////////////////////////////////////////////////////////////
		//(03.)RENVOYER L'OBJET CREE :
		/////////////////////////////////////////////////////////////
		return travailleurCree;
	}
}
