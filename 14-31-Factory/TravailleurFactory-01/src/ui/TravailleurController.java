package ui;

import java.util.Scanner;

import business.factory.TravailleurFactory;
import business.user.TravailleurUser;


/**
 * <b>CLASSE QUI REALISE LA NAVIGATION DANS L'INTERFACE UTILISATEUR RELATIVE A L'ENTITE 'ITravailleur'</b>
 * 
 * @author Tcharou
 *
 */
public class TravailleurController {

	
	/**
	 * <b>CONSTRUCTEUR SANS ARGUMENTS</b>
	 */
	public TravailleurController() {}
	
	
	/**
	 * <b>SEQUENCE PRINCIPALE</b>
	 */
	public void executerSequencePrincipale() {
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(01.)EFFECTUER UNE SAISIE :
		//     -->PARAMETRE A SAISIR : LE TYPE D'OBJET SOUHAITE 
		//////////////////////////////////////////////////////////////////////////////////////
		System.out.println("+--------------------------------------------------------------+");
		System.out.println("|           FABRIQUE DE TRAVAILLEURS                           |");
		System.out.println("+--------------------------------------------------------------+");
		System.out.print("| Veuillez saisir le type de travailleur souhait�, svp : ");
		Scanner scanner = new Scanner(System.in);
		String travailleurTypeSaisi = scanner.nextLine();
		scanner.close();
		System.out.println("+--------------------------------------------------------------+");
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(02.)CREER UNE FABRIQUE D'OBJETS :
		//     ->TYPE DE L'OBJET : 'TravailleurFactory'
		//////////////////////////////////////////////////////////////////////////////////////
		TravailleurFactory travailleurFactory = new TravailleurFactory();
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(03.)CONFIGURER LA FABRIQUE D'ANIMAUX :
		//     -->PARAMETRE A PASSER : LE TYPE D'OBJET A FABRIQUER 
		//////////////////////////////////////////////////////////////////////////////////////
		travailleurFactory.configure(travailleurTypeSaisi);
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(04.)CREER UN UTILISATEUR D'OBJETS :
		//     ->TYPE DE L'OBJET : 'TravailleurUser'
		//////////////////////////////////////////////////////////////////////////////////////
		TravailleurUser travailleurUser = new TravailleurUser(travailleurFactory);
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(05.)DEMANDER A L'UTILISATEUR D'OBJETS D'EFFECTUER SON TRAITEMENT 'work'
		//////////////////////////////////////////////////////////////////////////////////////
		travailleurUser.work();
	}
}
