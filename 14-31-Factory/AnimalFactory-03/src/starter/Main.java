package starter;

import java.util.Scanner;

import business.factory.AnimalFactory;
import business.user.AnimalUser;

public class Main {

	public static void main(String[] args) {

		//////////////////////////////////////////////////////////////////////////////////////
		//(01.)CREER UNE FABRIQUE D'ANIMAUX :
		//     ->TYPE DE L'OBJET : 'AnimalFactory'
		//////////////////////////////////////////////////////////////////////////////////////
		AnimalFactory animalFactory = new AnimalFactory();
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(02.)EFFECTUER UNE SAISIE :
		//     -->PARAMETRE A SAISIR : LE TYPE D'ANIMAL SOUHAITE 
		//////////////////////////////////////////////////////////////////////////////////////
		System.out.println("+--------------------------------------------------------------+");
		System.out.println("|           FABRIQUE D'ANIMAUX                                 |");
		System.out.println("+--------------------------------------------------------------+");
		System.out.print("| Veuillez saisir le type d'animal souhait�, svp : ");
		Scanner scanner = new Scanner(System.in);
		String animalType = scanner.nextLine();
		scanner.close();
		System.out.println("+--------------------------------------------------------------+");
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(03.)CONFIGURER LA FABRIQUE D'ANIMAUX :
		//     -->PARAMETRE A PASSER : LE TYPE D'ANIMAL A FABRIQUER 
		//////////////////////////////////////////////////////////////////////////////////////
		animalFactory.configure(animalType);
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(04.)CREER UN UTILISATEUR D'ANIMAUX :
		//     ->TYPE DE L'OBJET : 'AnimalUser'
		//////////////////////////////////////////////////////////////////////////////////////
		AnimalUser animalUser = new AnimalUser(animalFactory);
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(05.)DEMANDER A L'UTILISATEUR D'ANIMAUX D'EFFECTUER SON TRAITEMENT 'work'
		//////////////////////////////////////////////////////////////////////////////////////
		animalUser.work();
	}
}
