/**
 * 
 */
package domaine;

/**
 * @author Tcharou
 *
 */
public class Chien extends Animal {

	/**
	 * CONSTRUCTEUR SANS ARGUMENTS
	 */
	public Chien() {
		super();
	}

	/* (non-Javadoc)
	 * @see model.Animal#crier()
	 */
	@Override
	public void crier() {
		System.out.println("Ouaff !!");
	}

}
