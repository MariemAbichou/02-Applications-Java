package persistence.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

import business.exception.EntityAlreadyExistsException;
import business.exception.EntityNotFoundException;
import persistence.entity.Personne;
import persistence.exception.DAOException;


/**
 * <b>CLASSE DEFINISSANT LES OPERATIONS DE PERSISTANCE SUR L'ENTITE 'PERSONNE'</b>
 * <b>CLASSE CONCRETE NON GENERIQUE : ELLE S'APPLIQUE A L'ENTITE CONCRETE 'PERSONNE'</b>
 * 
 * @author BOUHBEL     Azzedine, 
 * @author DA ROCHA    Manuel, 
 * @author DESCHAMPS   Francis, 
 * @author DORE        Emeryck, 
 * @author DURINGER    Gaspard, 
 * @author EL FATEOUI  Najim, 
 * @author FLAMAND     Kevin, 
 * @author GARNIER     Thomas, 
 * @author LAVIGNON    Baptiste, 
 * @author MARTIN      Guillaume, 
 * @author MASSE       Auriane, 
 * @author MOHAMED     Amin, 
 * @author PLOUCHARD   C�dric, 
 * @author RAJAOMAZAVA Dina, 
 * @author SALAME      Alexandre, 
 * @author SILOTIA     Gilles, 
 * @author VUILLAUME   Laureen, 
 */
public class PersonneDAO extends AbstractDAO<Personne> {


	private static final String FILE = "data\\personne.csv";
	private static final int ENTITE_NOMBRE_D_ATTRIBUTS = 4;
	
	private static CounterDAO counterDAO;
	
	private static final String ENTITE                                                    = "Personne";

	private static final String FONCTIONNALITE_CREATION                                   = "Cr�ation";
	private static final String FONCTIONNALITE_RECHERCHE_PAR_IDENTIFIANT                  = "Recherche par identifiant";
	private static final String FONCTIONNALITE_RECHERCHE_PAR_EMAIL                        = "Recherche par email";
	private static final String FONCTIONNALITE_MISE_A_JOUR_AVEC_RECHERCHE_PAR_IDENTIFIANT = "Mise � jour avec recherche par identifiant";
	private static final String FONCTIONNALITE_SUPPRESSION_AVEC_RECHERCHE_PAR_IDENTIFIANT = "Supression avec recherche par identifiant";
	private static final String FONCTIONNALITE_DECOUPAGE_D_UNE_LIGNE_DE_FICHIER           = "D�coupage d'une ligne de fichier";
	private static final String FONCTIONNALITE_MODIFICATION_D_UNE_LIGNE_DE_FICHIER        = "Modification d'une ligne de fichier";
	private static final String FONCTIONNALITE_RAJOUT_D_UNE_LIGNE_DE_FICHIER              = "Rajout d'une ligne de fichier";
	
	private static final String ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE                      = "Ouverture du fichier �chou�e";
	private static final String ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE                      = "Fermeture du fichier �chou�e";
	private static final String ERREUR_ENTITE_EXISTE_DEJA                                 = "Entit� existe d�j�";
	private static final String ERREUR_ENTITE_INTROUVABLE                                 = "Entit� introuvable";
	private static final String ERREUR_LIGNE_DE_FICHIER_FORMAT_INCORRECT                  = "Ligne de fichier de format incorrect";

	
	/**
	 * <b>CONSTRUCTEUR SANS ARGUMENT</b>
	 */
	public PersonneDAO() {
		PersonneDAO.counterDAO = new CounterDAO();
	}
	
	@Override
	public Personne create(Personne pT) throws EntityAlreadyExistsException, EntityNotFoundException, DAOException {
		
		//////////////////////////////////////////////////////////////////////////////////
		//(01.)EFFECTUER UNE RECHERCHE SUR UN ATTRIBUT UNIQUE DE L'ENTITE A CREER :
		//     ->CHAMP : "eMail"
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)CAS D'ERREUR : ENTITE INTROUVABLE.
		//     (02.01.)RECUPERER UN IDENTIFIANT UNIQUE POUR L'ENTITE A CREER.
		//     (02.02.)METTRE A JOUR L'OBJET 'Personne' AVEC L'IDENTIFIANT UNIQUE RECUPERE.
		//     (02.03.)AJOUTER L'OBJET 'Personne' A LA FIN DU FICHIER.
		//     (02.04.)RENVOYER L'OBJET 'Personne' AJOUTE.
		//////////////////////////////////////////////////////////////////////////////////
		try {
			this.findByEMail(pT.getEMail());
			throw new EntityAlreadyExistsException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_CREATION + " -- " + PersonneDAO.ERREUR_ENTITE_EXISTE_DEJA);
			
		} catch (EntityNotFoundException enfe) {
			
			//////////////////////////////////////////////////////////////////////////////////
			//(02.01.)RECUPERER UN IDENTIFIANT UNIQUE POUR L'ENTITE A CREER.
			//////////////////////////////////////////////////////////////////////////////////
			long idFound = PersonneDAO.counterDAO.getUniqueId("personne");
			
			//////////////////////////////////////////////////////////////////////////////////
			//(02.02.)METTRE A JOUR L'OBJET 'Personne' AVEC L'IDENTIFIANT UNIQUE RECUPERE.
			//////////////////////////////////////////////////////////////////////////////////
			pT.setId(idFound);
			
			//////////////////////////////////////////////////////////////////////////////////
			//(02.03.)AJOUTER L'OBJET 'Personne' A LA FIN DU FICHIER.
			//////////////////////////////////////////////////////////////////////////////////
			Personne personneCreated = this.appendAtEOF(pT);
			
			//////////////////////////////////////////////////////////////////////////////////
			//(02.04.)RENVOYER L'OBJET 'Personne' AJOUTE.
			//////////////////////////////////////////////////////////////////////////////////
			return personneCreated;
		}
	}

	@Override
	public Personne findById(long pId) throws EntityNotFoundException, DAOException {

		//////////////////////////////////////////////////////////////////////////////////
		//(01.)OUVRIR LE FICHIER 'personne.csv'
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)SUR LE FICHIER 'personne.csv', EFFECTUER LE TRAITEMENT SUIVANT :
		//////////////////////////////////////////////////////////////////////////////////
		//     (02.01.)PARCOURIR LE FICHIER LIGNE PAR LIGNE.
		//     (02.02.)SUR CHAQUE LIGNE DU FICHIER, EFFECTUER LA SEQUENCE D'OPERATIONS SUIVANTE :
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
		//             (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Personne'.
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' RECUPERE.
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' FOURNI EN ARGUMENT.
		//                        SI CES 2 ELEMENTS SONT NON IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.03.01.)METTRE LA CONDITION DE SORTIE DE BOUCLE A 'FAUX
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.04.)CAS OU LES 2 ELEMENTS PRECEDENTS SONT IDENTIQUES : 
		//                        SI CES 2 ELEMENTS SONT IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.04.01.)RECUPERER LA POSITION ACTUELLE DU CURSEUR (DANS LE FICHIER).
		//                        (02.02.04.02.)QUITTER LA BOUCLE DE PARCOURS DU FICHIER. 
		//////////////////////////////////////////////////////////////////////////////////
		Personne personneFound = null;
		boolean hasBeenFound = false; 

		try(RandomAccessFile raf = new RandomAccessFile (new File (FILE), "r")) {
			
			raf.seek(0);
			
			do {
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
				//////////////////////////////////////////////////////////////////////////////////
				String ligneCourante = raf.readLine();
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Personne'.
				//////////////////////////////////////////////////////////////////////////////////
				if(ligneCourante == null) {
					hasBeenFound = false;
					break;
				}
				Personne personneCourante = this.transformLineToObject(ligneCourante);
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				if (personneCourante == null) {
					hasBeenFound = false;
					continue;
				}
				if (personneCourante.getId() != pId) {
					hasBeenFound = false;
					continue;
				}
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.04.)CAS OU LES 2 ELEMENTS SUIVANTS SONT IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				personneFound = personneCourante;
				hasBeenFound = true;
				break;
				
			} while (!hasBeenFound);
			
		} catch (FileNotFoundException e) {
			throw new DAOException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_RECHERCHE_PAR_IDENTIFIANT + " -- " + PersonneDAO.ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE);		
			
		} catch (IOException e1) {
			throw new DAOException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_RECHERCHE_PAR_IDENTIFIANT + " -- " + PersonneDAO.ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(03.)CAS D'ERREUR METIER SUIVANTE :
		//     LA LIGNE CONTENANT L'ENTITE COMPORTANT L'IDENTIFIANT RECHERCHE N'A PAS ETE TROUVEE:
		//     ->LANCER UNE EXCEPTION 'EntityNotFoundException'. 
		//////////////////////////////////////////////////////////////////////////////////
		if (hasBeenFound == false) {
			throw new EntityNotFoundException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_RECHERCHE_PAR_IDENTIFIANT + " -- " + PersonneDAO.ERREUR_ENTITE_INTROUVABLE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(04.)RENVOYER L'ELEMENT SUIVANT :
		//     ->L'OBJET 'Personne' TROUVE PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		return personneFound;
	}

	public Personne findByEMail(String pEMail) throws EntityNotFoundException, DAOException {

		//////////////////////////////////////////////////////////////////////////////////
		//(01.)OUVRIR LE FICHIER 'personne.csv'
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)SUR LE FICHIER 'personne.csv', EFFECTUER LE TRAITEMENT SUIVANT :
		//////////////////////////////////////////////////////////////////////////////////
		//     (02.01.)PARCOURIR LE FICHIER LIGNE PAR LIGNE.
		//     (02.02.)SUR CHAQUE LIGNE DU FICHIER, EFFECTUER LA SEQUENCE D'OPERATIONS SUIVANTE :
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
		//             (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Personne'.
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' RECUPERE.
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' FOURNI EN ARGUMENT.
		//                        SI CES 2 ELEMENTS SONT NON IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.03.01.)METTRE LA CONDITION DE SORTIE DE BOUCLE A 'FAUX
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.04.)CAS OU LES 2 ELEMENTS PRECEDENTS SONT IDENTIQUES : 
		//                        SI CES 2 ELEMENTS SONT IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.04.01.)RECUPERER LA POSITION ACTUELLE DU CURSEUR (DANS LE FICHIER).
		//                        (02.02.04.02.)QUITTER LA BOUCLE DE PARCOURS DU FICHIER. 
		//////////////////////////////////////////////////////////////////////////////////
		Personne personneFound = null;
		boolean hasBeenFound = false; 

		try(RandomAccessFile raf = new RandomAccessFile (new File (FILE), "r")) {
			
			raf.seek(0);
			
			do {
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
				//////////////////////////////////////////////////////////////////////////////////
				String ligneCourante = raf.readLine();
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Personne'.
				//////////////////////////////////////////////////////////////////////////////////
				if(ligneCourante == null) {
					hasBeenFound = false;
					break;
				}
				Personne personneCourante = this.transformLineToObject(ligneCourante);
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				if ((personneCourante == null) || (personneCourante.getEMail() == null)) {
					hasBeenFound = false;
					continue;
				}
				if (!personneCourante.getEMail().equals(pEMail)) {
					hasBeenFound = false;
					continue;
				}
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.04.)CAS OU LES 2 ELEMENTS SUIVANTS SONT IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				personneFound = personneCourante;
				hasBeenFound = true;
				break;
				
			} while (!hasBeenFound);
			
		} catch (FileNotFoundException e) {
			throw new DAOException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_RECHERCHE_PAR_EMAIL + " -- " + PersonneDAO.ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE);		
			
		} catch (IOException e1) {
			throw new DAOException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_RECHERCHE_PAR_EMAIL + " -- " + PersonneDAO.ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(03.)CAS D'ERREUR METIER SUIVANTE :
		//     LA LIGNE CONTENANT L'ENTITE COMPORTANT L'IDENTIFIANT RECHERCHE N'A PAS ETE TROUVEE:
		//     ->LANCER UNE EXCEPTION 'EntityNotFoundException'. 
		//////////////////////////////////////////////////////////////////////////////////
		if (hasBeenFound == false) {
			throw new EntityNotFoundException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_RECHERCHE_PAR_EMAIL + " -- " + PersonneDAO.ERREUR_ENTITE_INTROUVABLE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(04.)RENVOYER L'ELEMENT SUIVANT :
		//     ->L'OBJET 'Personne' TROUVE PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		return personneFound;
	}
	
	@Override
	public List<Personne> findList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Personne updateById(Personne pT) throws EntityNotFoundException, DAOException {
		
		//////////////////////////////////////////////////////////////////////////////////
		//(01.)OUVRIR LE FICHIER 'personne.csv'
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)SUR LE FICHIER 'personne.csv', EFFECTUER LE TRAITEMENT SUIVANT :
		//////////////////////////////////////////////////////////////////////////////////
		//     (02.01.)PARCOURIR LE FICHIER LIGNE PAR LIGNE.
		//     (02.02.)SUR CHAQUE LIGNE DU FICHIER, EFFECTUER LA SEQUENCE D'OPERATIONS SUIVANTE :
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
		//             (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Personne'.
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' RECUPERE.
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' FOURNI EN ARGUMENT.
		//                        SI CES 2 ELEMENTS SONT NON IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.03.01.)METTRE LA CONDITION DE SORTIE DE BOUCLE A 'FAUX
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.04.)CAS OU LES 2 ELEMENTS PRECEDENTS SONT IDENTIQUES : 
		//                        SI CES 2 ELEMENTS SONT IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.04.01.)RECUPERER LA POSITION ACTUELLE DU CURSEUR (DANS LE FICHIER).
		//                        (02.02.04.02.)QUITTER LA BOUCLE DE PARCOURS DU FICHIER. 
		//////////////////////////////////////////////////////////////////////////////////
		long positionFound = 0;
		String ligneFound = null;
		boolean hasBeenFound = false; 

		try(RandomAccessFile raf = new RandomAccessFile (new File (FILE), "r")) {
			
			raf.seek(0);
			
			do {
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
				//////////////////////////////////////////////////////////////////////////////////
				String ligneCourante = raf.readLine();
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Personne'.
				//////////////////////////////////////////////////////////////////////////////////
				if((ligneCourante == null) || (ligneCourante.isEmpty())) {
					hasBeenFound = false;
					break;
				}
				Personne personneCourante = this.transformLineToObject(ligneCourante);
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				if ((personneCourante == null) || (pT == null)) {
					hasBeenFound = false;
					continue;
				}
				if (personneCourante.getId() != pT.getId()) {
					hasBeenFound = false;
					continue;
				}
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.04.)CAS OU LES 2 ELEMENTS SUIVANTS SONT IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				positionFound = raf.getFilePointer();
				ligneFound = ligneCourante;
				hasBeenFound = true;
				break;
				
			} while (!hasBeenFound);
			
		} catch (FileNotFoundException e) {
			throw new DAOException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_MISE_A_JOUR_AVEC_RECHERCHE_PAR_IDENTIFIANT + " -- " + PersonneDAO.ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE);		
			
		} catch (IOException e1) {
			throw new DAOException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_MISE_A_JOUR_AVEC_RECHERCHE_PAR_IDENTIFIANT + " -- " + PersonneDAO.ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(03.)CAS D'ERREUR METIER SUIVANTE :
		//     LA LIGNE CONTENANT L'ENTITE COMPORTANT L'IDENTIFIANT RECHERCHE N'A PAS ETE TROUVEE:
		//     ->LANCER UNE EXCEPTION 'EntityNotFoundException'. 
		//////////////////////////////////////////////////////////////////////////////////
		if (hasBeenFound == false) {
			throw new EntityNotFoundException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_MISE_A_JOUR_AVEC_RECHERCHE_PAR_IDENTIFIANT + " -- " + PersonneDAO.ERREUR_ENTITE_INTROUVABLE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(04.)CALCULER LA POSITION (DANS LE FICHIER) DU DEBUT DE LA LIGNE RECUPEREE.
		//     UTILISER POUR CELA LES ELEMENTS SUIVANTS :
		//     ->LA POSITION RECUPEREE PRECEDEMMENT. 
		//     ->LIGNE RECUPEREE PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		positionFound -= (ligneFound.length() + 1);
		
		//////////////////////////////////////////////////////////////////////////////////
		//(05.)METTRE A JOUR UNE LIGNE DU FICHIER EN UTILISANT LES PARAMETRES SUIVANTS:
		//     ->LA POSITION CALCULEE PRECEDEMMENT. 
		//     ->L'OBJET 'Personne' MIS A JOUR PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		this.updateAtPosition(positionFound, pT);
		
		//////////////////////////////////////////////////////////////////////////////////
		//(06.)RENVOYER L'ELEMENT SUIVANT :
		//     ->L'OBJET 'Personne' MIS A JOUR PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		return pT;
	}

	@Override
	public Personne deleteById(long pId) throws EntityNotFoundException, DAOException {
		
		//////////////////////////////////////////////////////////////////////////////////
		//(01.)OUVRIR LE FICHIER 'personne.csv'
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)SUR LE FICHIER 'personne.csv', EFFECTUER LE TRAITEMENT SUIVANT :
		//////////////////////////////////////////////////////////////////////////////////
		//     (02.01.)PARCOURIR LE FICHIER LIGNE PAR LIGNE.
		//     (02.02.)SUR CHAQUE LIGNE DU FICHIER, EFFECTUER LA SEQUENCE D'OPERATIONS SUIVANTE :
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
		//             (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Personne'.
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' RECUPERE.
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' FOURNI EN ARGUMENT.
		//                        SI CES 2 ELEMENTS SONT NON IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.03.01.)METTRE LA CONDITION DE SORTIE DE BOUCLE A 'FAUX
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.04.)CAS OU LES 2 ELEMENTS PRECEDENTS SONT IDENTIQUES : 
		//                        SI CES 2 ELEMENTS SONT IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.04.01.)RECUPERER LA POSITION ACTUELLE DU CURSEUR (DANS LE FICHIER).
		//                        (02.02.04.02.)QUITTER LA BOUCLE DE PARCOURS DU FICHIER. 
		//////////////////////////////////////////////////////////////////////////////////
		long positionFound = 0;
		String ligneFound = null;
		Personne personneFound = null;
		boolean hasBeenFound = false; 

		try(RandomAccessFile raf = new RandomAccessFile (new File (FILE), "r")) {
			
			raf.seek(0);
			
			do {
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
				//////////////////////////////////////////////////////////////////////////////////
				String ligneCourante = raf.readLine();
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Personne'.
				//////////////////////////////////////////////////////////////////////////////////
				if((ligneCourante == null) || (ligneCourante.isEmpty())) {
					hasBeenFound = false;
					break;
				}
				Personne personneCourante = this.transformLineToObject(ligneCourante);
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				if (personneCourante == null) {
					hasBeenFound = false;
					continue;
				}
				if (personneCourante.getId() != pId) {
					hasBeenFound = false;
					continue;
				}
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.04.)CAS OU LES 2 ELEMENTS SUIVANTS SONT IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Personne' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				positionFound = raf.getFilePointer();
				ligneFound = ligneCourante;
				personneFound = personneCourante;
				hasBeenFound = true;
				break;
				
			} while (!hasBeenFound);
			
		} catch (FileNotFoundException e) {
			throw new DAOException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_MISE_A_JOUR_AVEC_RECHERCHE_PAR_IDENTIFIANT + " -- " + PersonneDAO.ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE);		
			
		} catch (IOException e1) {
			throw new DAOException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_MISE_A_JOUR_AVEC_RECHERCHE_PAR_IDENTIFIANT + " -- " + PersonneDAO.ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(03.)CAS D'ERREUR METIER SUIVANTE :
		//     LA LIGNE CONTENANT L'ENTITE COMPORTANT L'IDENTIFIANT RECHERCHE N'A PAS ETE TROUVEE:
		//     ->LANCER UNE EXCEPTION 'EntityNotFoundException'. 
		//////////////////////////////////////////////////////////////////////////////////
		if (hasBeenFound == false) {
			throw new EntityNotFoundException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_MISE_A_JOUR_AVEC_RECHERCHE_PAR_IDENTIFIANT + " -- " + PersonneDAO.ERREUR_ENTITE_INTROUVABLE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(04.)CALCULER LA POSITION (DANS LE FICHIER) DU DEBUT DE LA LIGNE RECUPEREE.
		//     UTILISER POUR CELA LES ELEMENTS SUIVANTS :
		//     ->LA POSITION RECUPEREE PRECEDEMMENT. 
		//     ->LIGNE RECUPEREE PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		positionFound -= (ligneFound.length() + 1);
		
		//////////////////////////////////////////////////////////////////////////////////
		//(05.)SUPPRIMER (DANS LE FICHIER) LA LIGNE DECRITE CI-DESSOUS :
		//     ->DEBUT DE LA LIGNE    : LA POSITION CALCULEE PRECEDEMMENT.
		//     ->FIN DE LA LIGNE : LE RETOUR CHARIOT.
		//////////////////////////////////////////////////////////////////////////////////
		this.deleteLineFromLineBegin(positionFound);
		
		//////////////////////////////////////////////////////////////////////////////////
		//(06.)RENVOYER L'OBJET 'Personne' TROUVE PRECEDEMMENT.
		//////////////////////////////////////////////////////////////////////////////////
		return personneFound;
	}

	@Override
	protected Personne transformLineToObject(String pLine) throws DAOException {
		
		///////////////////////////////////////////////////
		//(01.)DECOUPER LA LIGNE FOURNIE 
		///////////////////////////////////////////////////
		String[] proprietes = pLine.split(";");
		
		///////////////////////////////////////////////////
		//(02.)TRAITER LE CAS D'ERREUR : 
		//     PARMI LES 2 NOMBRES SUIVANTS, LE 1ER EST INFERIEUR AU 2EME. 
		//     ->LE NOMBRE D'ELEMENTS DANS LE TABLEAU 'proprietes' 
		//     ->LE NOMBRE D'ATTRIBUTS DANS L'ENTITE.
		///////////////////////////////////////////////////
		if ((proprietes == null) || (proprietes.length < PersonneDAO.ENTITE_NOMBRE_D_ATTRIBUTS)) {
			throw new DAOException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_DECOUPAGE_D_UNE_LIGNE_DE_FICHIER + " -- " + PersonneDAO.ERREUR_LIGNE_DE_FICHIER_FORMAT_INCORRECT);
		}
		///////////////////////////////////////////////////
		//(02.)CREER ET ALIMENTER LES PROPRIETES D'UN OBJET 'Personne'
		///////////////////////////////////////////////////
		long personneId = Long.parseUnsignedLong(proprietes[0]);
		String personneEMail = proprietes[1];
		String personneNom = proprietes[2];
		String personnePrenom = proprietes[3];
		
		///////////////////////////////////////////////////
		//(03.)CREER ET ALIMENTER UN OBJET 'Personne'
		///////////////////////////////////////////////////
		Personne personne = new Personne(personneId, personneEMail, personneNom, personnePrenom);
		
		///////////////////////////////////////////////////
		//(04.)RENVOYER L'OBJET 'Personne'
		///////////////////////////////////////////////////
		return personne;
	}
	
	@Override
	protected Personne updateAtPosition(long pPosition, Personne pT) throws DAOException {

		//////////////////////////////////////////////////////////////////////////////////
		//(01.)OUVRIR LE FICHIER 'personne.csv'
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)SUR LE FICHIER 'personne.csv', EFFECTUER LE TRAITEMENT NOMINAL SUIVANT :
		//////////////////////////////////////////////////////////////////////////////////
		//     (02.01.)POSITIONNER LE CURSEUR A LA POSITION FOURNIE EN ARGUMENT.
		//     (02.02.)ECRIRE UNE LIGNE DANS LE FICHIER CONFORMEMENT AUX EXIGENCES SUIVANTES:
		//             ->ECRIRE A LA POSITION ACTUELLE.
		//             ->ECRIRE EN UTILISANT LA CHAINE DE CARACTERE PRODUITE PAR L'OBJET 'Personne' FOURNI.
		//             ->ECRIRE UN RETOUR CHARIOT APRES LA FIN DE LA LIGNE.
		//////////////////////////////////////////////////////////////////////////////////
		try(RandomAccessFile raf = new RandomAccessFile (new File (FILE), "rw")) {
			
			//////////////////////////////////////////////////////////////////////////////////
			// (02.01.)POSITIONNER LE CURSEUR A LA POSITION FOURNIE EN ARGUMENT.
			//////////////////////////////////////////////////////////////////////////////////
			raf.seek(pPosition);
			
			//////////////////////////////////////////////////////////////////////////////////
			// (02.02.)ECRIRE UNE LIGNE DANS LE FICHIER CONFORMEMENT AUX EXIGENCES SUIVANTES:
			//         ->ECRIRE A LA POSITION ACTUELLE.
			//         ->ECRIRE EN UTILISANT LA CHAINE DE CARACTERE PRODUITE PAR L'OBJET 'Personne' FOURNI.
			//         ->ECRIRE UN RETOUR CHARIOT APRES LA FIN DE LA LIGNE.
			//////////////////////////////////////////////////////////////////////////////////
			raf.writeBytes(pT.toString());
			
		} catch (FileNotFoundException e) {
			throw new DAOException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_MODIFICATION_D_UNE_LIGNE_DE_FICHIER + " -- " + PersonneDAO.ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE);		
			
		} catch (IOException e1) {
			throw new DAOException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_MODIFICATION_D_UNE_LIGNE_DE_FICHIER + " -- " + PersonneDAO.ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(03.)RENVOYER L'OBJET 'Personne' MODIFIE PRECEDEMMENT.
		//////////////////////////////////////////////////////////////////////////////////
		return pT;		
	}
	
	@Override
	protected Personne appendAtEOF(Personne pT) throws DAOException {

		//////////////////////////////////////////////////////////////////////////////////
		//(01.)OUVRIR LE FICHIER 'personne.csv'
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)SUR LE FICHIER 'personne.csv', EFFECTUER LE TRAITEMENT NOMINAL SUIVANT :
		//////////////////////////////////////////////////////////////////////////////////
		//     (02.01.)POSITIONNER LE CURSEUR A LA POSITION FOURNIE EN ARGUMENT.
		//     (02.02.)ECRIRE UNE LIGNE DANS LE FICHIER CONFORMEMENT AUX EXIGENCES SUIVANTES:
		//             ->ECRIRE A LA POSITION ACTUELLE.
		//             ->ECRIRE EN UTILISANT LA CHAINE DE CARACTERE PRODUITE PAR L'OBJET 'Personne' FOURNI.
		//             ->ECRIRE UN RETOUR CHARIOT APRES LA FIN DE LA LIGNE.
		//////////////////////////////////////////////////////////////////////////////////
		try(RandomAccessFile raf = new RandomAccessFile (new File (FILE), "rw")) {
			
			//////////////////////////////////////////////////////////////////////////////////
			// (02.01.)POSITIONNER LE CURSEUR A LA POSITION 'FIN DE FICHIER'.
			//////////////////////////////////////////////////////////////////////////////////
			long rafLength = raf.length();
			raf.seek(rafLength);
			
			//////////////////////////////////////////////////////////////////////////////////
			// (02.02.)ECRIRE UNE LIGNE DANS LE FICHIER CONFORMEMENT AUX EXIGENCES SUIVANTES:
			//         ->ECRIRE A LA POSITION ACTUELLE.
			//         ->ECRIRE EN UTILISANT LA CHAINE DE CARACTERE PRODUITE PAR L'OBJET 'Personne' FOURNI.
			//         ->ECRIRE UN RETOUR CHARIOT APRES LA FIN DE LA LIGNE.
			//////////////////////////////////////////////////////////////////////////////////
			if (rafLength > 0) {
				raf.writeByte('\n');
			}
			raf.writeBytes(pT.toString());
			
		} catch (FileNotFoundException e) {
			throw new DAOException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_RAJOUT_D_UNE_LIGNE_DE_FICHIER + " -- " + PersonneDAO.ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE);		
			
		} catch (IOException e1) {
			throw new DAOException(PersonneDAO.ENTITE + " -- " + PersonneDAO.FONCTIONNALITE_RAJOUT_D_UNE_LIGNE_DE_FICHIER + " -- " + PersonneDAO.ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(03.)RENVOYER L'OBJET 'Personne' MODIFIE PRECEDEMMENT.
		//////////////////////////////////////////////////////////////////////////////////
		return pT;		
	}
	
	@Override
	protected long deleteLineFromLineBegin(long pPosition) throws DAOException {
		
	}

}
