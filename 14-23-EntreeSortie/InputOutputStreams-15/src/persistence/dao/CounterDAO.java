package persistence.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

import business.exception.EntityAlreadyExistsException;
import business.exception.EntityNotFoundException;
import persistence.entity.Counter;
import persistence.exception.DAOException;

/**
 * <b>CLASSE DEFINISSANT LES OPERATIONS DE PERSISTANCE SUR L'ENTITE 'COUNTER'</b>
 * <br><b>CLASSE CONCRETE NON GENERIQUE : ELLE S'APPLIQUE A L'ENTITE CONCRETE 'COUNTER'</b>
 * 
 * @author BOUHBEL     Azzedine, 
 * @author DA ROCHA    Manuel, 
 * @author DESCHAMPS   Francis, 
 * @author DORE        Emeryck, 
 * @author DURINGER    Gaspard, 
 * @author EL FATEOUI  Najim, 
 * @author FLAMAND     Kevin, 
 * @author GARNIER     Thomas, 
 * @author LAVIGNON    Baptiste, 
 * @author MARTIN      Guillaume, 
 * @author MASSE       Auriane, 
 * @author MOHAMED     Amin, 
 * @author PLOUCHARD   C�dric, 
 * @author RAJAOMAZAVA Dina, 
 * @author SALAME      Alexandre, 
 * @author SILOTIA     Gilles, 
 * @author VUILLAUME   Laureen, 
 */
public class CounterDAO extends AbstractDAO<Counter> {


	private static final String FILE = "data\\counter.csv";
	private static final int ENTITE_NOMBRE_D_ATTRIBUTS = 3;
	private static final String ENTITE                                                    = "Counter";

	private static final String FONCTIONNALITE_CREATION                                   = "Cr�ation";
	private static final String FONCTIONNALITE_RECHERCHE_PAR_IDENTIFIANT                  = "Recherche par identifiant";
	private static final String FONCTIONNALITE_RECHERCHE_PAR_NOM                          = "Recherche par nom";
	private static final String FONCTIONNALITE_MISE_A_JOUR_AVEC_RECHERCHE_PAR_IDENTIFIANT = "Mise � jour avec recherche par identifiant";
	private static final String FONCTIONNALITE_SUPPRESSION_AVEC_RECHERCHE_PAR_IDENTIFIANT = "Suppression avec recherche par identifiant";
	private static final String FONCTIONNALITE_RECUPERATION_D_UN_IDENTIFIANT_UNIQUE       = "R�cup�ration d'un identifiant unique";
	private static final String FONCTIONNALITE_DECOUPAGE_D_UNE_LIGNE_DE_FICHIER           = "D�coupage d'une ligne de fichier";
	private static final String FONCTIONNALITE_MODIFICATION_D_UNE_LIGNE_DE_FICHIER        = "Modification d'une ligne de fichier";
	private static final String FONCTIONNALITE_RAJOUT_D_UNE_LIGNE_DE_FICHIER              = "Rajout d'une ligne de fichier";
	
	private static final String ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE                      = "Ouverture du fichier �chou�e";
	private static final String ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE                      = "Fermeture du fichier �chou�e";
	private static final String ERREUR_ENTITE_EXISTE_DEJA                                 = "Entit� existe d�j�";
	private static final String ERREUR_ENTITE_INTROUVABLE                                 = "Entit� introuvable";
	private static final String ERREUR_LIGNE_DE_FICHIER_FORMAT_INCORRECT                  = "Ligne de fichier de format incorrect";
	
	
	@Override
	public Counter create(Counter pT) throws EntityAlreadyExistsException, EntityNotFoundException, DAOException {

		//////////////////////////////////////////////////////////////////////////////////
		//(01.)EFFECTUER UNE RECHERCHE SUR UN ATTRIBUT UNIQUE DE L'ENTITE A CREER :
		//     ->CHAMP : "nom"
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)CAS D'ERREUR : ENTITE INTROUVABLE.
		//     (02.01.)RECUPERER UN IDENTIFIANT UNIQUE POUR L'ENTITE A CREER.
		//     (02.02.)METTRE A JOUR L'OBJET 'Counter' AVEC L'IDENTIFIANT UNIQUE RECUPERE.
		//     (02.03.)AJOUTER L'OBJET 'Counter' A LA FIN DU FICHIER.
		//     (02.04.)RENVOYER L'OBJET 'Counter' AJOUTE.
		//////////////////////////////////////////////////////////////////////////////////
		try {
			this.findByNom(pT.getNom());
			throw new EntityAlreadyExistsException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_CREATION + " -- " + CounterDAO.ERREUR_ENTITE_EXISTE_DEJA);
			
		} catch (EntityNotFoundException enfe) {
			
			//////////////////////////////////////////////////////////////////////////////////
			//(02.01.)RECUPERER UN IDENTIFIANT UNIQUE POUR L'ENTITE A CREER.
			//////////////////////////////////////////////////////////////////////////////////
			long idFound = this.getUniqueId("counter");
			
			//////////////////////////////////////////////////////////////////////////////////
			//(02.02.)METTRE A JOUR L'OBJET 'Counter' AVEC L'IDENTIFIANT UNIQUE RECUPERE.
			//////////////////////////////////////////////////////////////////////////////////
			pT.setId(idFound);
			
			//////////////////////////////////////////////////////////////////////////////////
			//(02.03.)AJOUTER L'OBJET 'Counter' A LA FIN DU FICHIER.
			//////////////////////////////////////////////////////////////////////////////////
			Counter counterCreated = this.appendAtEOF(pT);
			
			//////////////////////////////////////////////////////////////////////////////////
			//(02.04.)RENVOYER L'OBJET 'Counter' AJOUTE.
			//////////////////////////////////////////////////////////////////////////////////
			return counterCreated;
		}
	}

	@Override
	public Counter findById(long pId) throws EntityNotFoundException, DAOException {
		
		//////////////////////////////////////////////////////////////////////////////////
		//(01.)OUVRIR LE FICHIER 'counter.csv'
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)SUR LE FICHIER 'counter.csv', EFFECTUER LE TRAITEMENT SUIVANT :
		//////////////////////////////////////////////////////////////////////////////////
		//     (02.01.)PARCOURIR LE FICHIER LIGNE PAR LIGNE.
		//     (02.02.)SUR CHAQUE LIGNE DU FICHIER, EFFECTUER LA SEQUENCE D'OPERATIONS SUIVANTE :
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
		//             (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Counter'.
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' RECUPERE.
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' FOURNI EN ARGUMENT.
		//                        SI CES 2 ELEMENTS SONT NON IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.03.01.)METTRE LA CONDITION DE SORTIE DE BOUCLE A 'FAUX
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.04.)CAS OU LES 2 ELEMENTS PRECEDENTS SONT IDENTIQUES : 
		//                        SI CES 2 ELEMENTS SONT IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.04.01.)RECUPERER LA POSITION ACTUELLE DU CURSEUR (DANS LE FICHIER).
		//                        (02.02.04.02.)QUITTER LA BOUCLE DE PARCOURS DU FICHIER. 
		//////////////////////////////////////////////////////////////////////////////////
		Counter counterFound = null;
		boolean hasBeenFound = false; 

		try(RandomAccessFile raf = new RandomAccessFile (new File (FILE), "r")) {
			
			raf.seek(0);
			
			do {
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
				//////////////////////////////////////////////////////////////////////////////////
				String ligneCourante = raf.readLine();
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Counter'.
				//////////////////////////////////////////////////////////////////////////////////
				if(ligneCourante == null) {
					hasBeenFound = false;
					break;
				}
				Counter counterCourant = this.transformLineToObject(ligneCourante);
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				if (counterCourant == null) {
					hasBeenFound = false;
					continue;
				}
				if (counterCourant.getId() != pId) {
					hasBeenFound = false;
					continue;
				}
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.04.)CAS OU LES 2 ELEMENTS SUIVANTS SONT IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				counterFound = counterCourant;
				hasBeenFound = true;
				break;
				
			} while (!hasBeenFound);
			
		} catch (FileNotFoundException e) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_RECHERCHE_PAR_IDENTIFIANT + " -- " + CounterDAO.ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE);		
			
		} catch (IOException e1) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_RECHERCHE_PAR_IDENTIFIANT + " -- " + CounterDAO.ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(03.)CAS D'ERREUR METIER SUIVANTE :
		//     LA LIGNE CONTENANT L'ENTITE COMPORTANT L'IDENTIFIANT RECHERCHE N'A PAS ETE TROUVEE:
		//     ->LANCER UNE EXCEPTION 'EntityNotFoundException'. 
		//////////////////////////////////////////////////////////////////////////////////
		if (hasBeenFound == false) {
			throw new EntityNotFoundException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_RECHERCHE_PAR_IDENTIFIANT + " -- " + CounterDAO.ERREUR_ENTITE_INTROUVABLE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(04.)RENVOYER L'ELEMENT SUIVANT :
		//     ->L'OBJET 'Counter' TROUVE PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		return counterFound;
	}

	public Counter findByNom(String pNom) throws EntityNotFoundException, DAOException {
		
		//////////////////////////////////////////////////////////////////////////////////
		//(01.)OUVRIR LE FICHIER 'counter.csv'
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)SUR LE FICHIER 'counter.csv', EFFECTUER LE TRAITEMENT SUIVANT :
		//////////////////////////////////////////////////////////////////////////////////
		//     (02.01.)PARCOURIR LE FICHIER LIGNE PAR LIGNE.
		//     (02.02.)SUR CHAQUE LIGNE DU FICHIER, EFFECTUER LA SEQUENCE D'OPERATIONS SUIVANTE :
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
		//             (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Counter'.
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' RECUPERE.
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' FOURNI EN ARGUMENT.
		//                        SI CES 2 ELEMENTS SONT NON IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.03.01.)METTRE LA CONDITION DE SORTIE DE BOUCLE A 'FAUX
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.04.)CAS OU LES 2 ELEMENTS PRECEDENTS SONT IDENTIQUES : 
		//                        SI CES 2 ELEMENTS SONT IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.04.01.)RECUPERER LA POSITION ACTUELLE DU CURSEUR (DANS LE FICHIER).
		//                        (02.02.04.02.)QUITTER LA BOUCLE DE PARCOURS DU FICHIER. 
		//////////////////////////////////////////////////////////////////////////////////
		Counter counterFound = null;
		boolean hasBeenFound = false; 

		try(RandomAccessFile raf = new RandomAccessFile (new File (FILE), "r")) {
			
			raf.seek(0);
			
			do {
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
				//////////////////////////////////////////////////////////////////////////////////
				String ligneCourante = raf.readLine();
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Counter'.
				//////////////////////////////////////////////////////////////////////////////////
				if(ligneCourante == null) {
					hasBeenFound = false;
					break;
				}
				Counter counterCourant = this.transformLineToObject(ligneCourante);
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				if ((counterCourant == null) || (counterCourant.getNom() == null)) {
					hasBeenFound = false;
					continue;
				}
				if (!counterCourant.getNom().equals(pNom)) {
					hasBeenFound = false;
					continue;
				}
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.04.)CAS OU LES 2 ELEMENTS SUIVANTS SONT IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				counterFound = counterCourant;
				hasBeenFound = true;
				break;
				
			} while (!hasBeenFound);
			
		} catch (FileNotFoundException e) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_RECHERCHE_PAR_NOM + " -- " + CounterDAO.ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE);		
			
		} catch (IOException e1) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_RECHERCHE_PAR_NOM + " -- " + CounterDAO.ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(03.)CAS D'ERREUR METIER SUIVANTE :
		//     LA LIGNE CONTENANT L'ENTITE COMPORTANT L'IDENTIFIANT RECHERCHE N'A PAS ETE TROUVEE:
		//     ->LANCER UNE EXCEPTION 'EntityNotFoundException'. 
		//////////////////////////////////////////////////////////////////////////////////
		if (hasBeenFound == false) {
			throw new EntityNotFoundException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_RECHERCHE_PAR_NOM + " -- " + CounterDAO.ERREUR_ENTITE_INTROUVABLE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(04.)RENVOYER L'ELEMENT SUIVANT :
		//     ->L'OBJET 'Counter' TROUVE PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		return counterFound;
	}

	@Override
	public List<Counter> findList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Counter updateById(Counter pT) throws EntityNotFoundException, DAOException {
		
		//////////////////////////////////////////////////////////////////////////////////
		//(01.)OUVRIR LE FICHIER 'counter.csv'
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)SUR LE FICHIER 'counter.csv', EFFECTUER LE TRAITEMENT SUIVANT :
		//////////////////////////////////////////////////////////////////////////////////
		//     (02.01.)PARCOURIR LE FICHIER LIGNE PAR LIGNE.
		//     (02.02.)SUR CHAQUE LIGNE DU FICHIER, EFFECTUER LA SEQUENCE D'OPERATIONS SUIVANTE :
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
		//             (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Counter'.
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' RECUPERE.
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' FOURNI EN ARGUMENT.
		//                        SI CES 2 ELEMENTS SONT NON IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.03.01.)METTRE LA CONDITION DE SORTIE DE BOUCLE A 'FAUX
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.04.)CAS OU LES 2 ELEMENTS PRECEDENTS SONT IDENTIQUES : 
		//                        SI CES 2 ELEMENTS SONT IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.04.01.)RECUPERER LA POSITION ACTUELLE DU CURSEUR (DANS LE FICHIER).
		//                        (02.02.04.02.)QUITTER LA BOUCLE DE PARCOURS DU FICHIER. 
		//////////////////////////////////////////////////////////////////////////////////
		long positionFound = 0;
		String ligneFound = null;
		boolean hasBeenFound = false; 

		try(RandomAccessFile raf = new RandomAccessFile (new File (FILE), "r")) {
			
			raf.seek(0);
			
			do {
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
				//////////////////////////////////////////////////////////////////////////////////
				String ligneCourante = raf.readLine();
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Counter'.
				//////////////////////////////////////////////////////////////////////////////////
				if(ligneCourante == null) {
					hasBeenFound = false;
					break;
				}
				Counter counterCourant = this.transformLineToObject(ligneCourante);
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				if ((counterCourant == null) || (pT == null)) {
					hasBeenFound = false;
					continue;
				}
				if (counterCourant.getId() != pT.getId()) {
					hasBeenFound = false;
					continue;
				}
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.04.)CAS OU LES 2 ELEMENTS SUIVANTS SONT IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				positionFound = raf.getFilePointer();
				ligneFound = ligneCourante;
				hasBeenFound = true;
				break;
				
			} while (!hasBeenFound);
			
		} catch (FileNotFoundException e) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_MISE_A_JOUR_AVEC_RECHERCHE_PAR_IDENTIFIANT + " -- " + CounterDAO.ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE);		
			
		} catch (IOException e1) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_MISE_A_JOUR_AVEC_RECHERCHE_PAR_IDENTIFIANT + " -- " + CounterDAO.ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(03.)CAS D'ERREUR METIER SUIVANTE :
		//     LA LIGNE CONTENANT L'ENTITE COMPORTANT L'IDENTIFIANT RECHERCHE N'A PAS ETE TROUVEE:
		//     ->LANCER UNE EXCEPTION 'EntityNotFoundException'. 
		//////////////////////////////////////////////////////////////////////////////////
		if (hasBeenFound == false) {
			throw new EntityNotFoundException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_MISE_A_JOUR_AVEC_RECHERCHE_PAR_IDENTIFIANT + " -- " + CounterDAO.ERREUR_ENTITE_INTROUVABLE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(04.)CALCULER LA POSITION (DANS LE FICHIER) DU DEBUT DE LA LIGNE RECUPEREE.
		//     UTILISER POUR CELA LES ELEMENTS SUIVANTS :
		//     ->LA POSITION RECUPEREE PRECEDEMMENT. 
		//     ->LIGNE RECUPEREE PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		positionFound -= (ligneFound.length() + 1);
		
		//////////////////////////////////////////////////////////////////////////////////
		//(05.)METTRE A JOUR UNE LIGNE DU FICHIER EN UTILISANT LES PARAMETRES SUIVANTS:
		//     ->LA POSITION CALCULEE PRECEDEMMENT. 
		//     ->L'OBJET 'Counter' MIS A JOUR PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		this.updateAtPosition(positionFound, pT);
		
		//////////////////////////////////////////////////////////////////////////////////
		//(06.)RENVOYER L'ELEMENT SUIVANT :
		//     ->L'OBJET 'Counter' MIS A JOUR PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		return pT;	
	}

	@Override
	public Counter deleteById(long pId) throws EntityNotFoundException, DAOException {
		
		//////////////////////////////////////////////////////////////////////////////////
		//(01.)OUVRIR LE FICHIER 'counter.csv'
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)SUR LE FICHIER 'counter.csv', EFFECTUER LE TRAITEMENT SUIVANT :
		//////////////////////////////////////////////////////////////////////////////////
		//     (02.01.)PARCOURIR LE FICHIER LIGNE PAR LIGNE.
		//     (02.02.)SUR CHAQUE LIGNE DU FICHIER, EFFECTUER LA SEQUENCE D'OPERATIONS SUIVANTE :
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
		//             (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Counter'.
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' RECUPERE.
		//                        ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' FOURNI EN ARGUMENT.
		//                        SI CES 2 ELEMENTS SONT NON IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.03.01.)METTRE LA CONDITION DE SORTIE DE BOUCLE A 'FAUX
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.04.)CAS OU LES 2 ELEMENTS PRECEDENTS SONT IDENTIQUES : 
		//                        SI CES 2 ELEMENTS SONT IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.04.01.)RECUPERER LA POSITION ACTUELLE DU CURSEUR (DANS LE FICHIER).
		//                        (02.02.04.02.)QUITTER LA BOUCLE DE PARCOURS DU FICHIER. 
		//////////////////////////////////////////////////////////////////////////////////
		long positionFound = 0;
		String ligneFound = null;
		Counter counterFound = null;
		boolean hasBeenFound = false; 

		try(RandomAccessFile raf = new RandomAccessFile (new File (FILE), "r")) {
			
			raf.seek(0);
			
			do {
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
				//////////////////////////////////////////////////////////////////////////////////
				String ligneCourante = raf.readLine();
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Counter'.
				//////////////////////////////////////////////////////////////////////////////////
				if(ligneCourante == null) {
					hasBeenFound = false;
					break;
				}
				Counter counterCourant = this.transformLineToObject(ligneCourante);
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				if (counterCourant == null) {
					hasBeenFound = false;
					continue;
				}
				if (counterCourant.getId() != pId) {
					hasBeenFound = false;
					continue;
				}
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.04.)CAS OU LES 2 ELEMENTS SUIVANTS SONT IDENTIQUES : 
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' RECUPERE.
				//            ->L'ATTRIBUT 'id' DE L'OBJET 'Counter' FOURNI EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				positionFound = raf.getFilePointer();
				ligneFound = ligneCourante;
				counterFound = counterCourant;
				hasBeenFound = true;
				break;
				
			} while (!hasBeenFound);
			
		} catch (FileNotFoundException e) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_MISE_A_JOUR_AVEC_RECHERCHE_PAR_IDENTIFIANT + " -- " + CounterDAO.ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE);		
			
		} catch (IOException e1) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_MISE_A_JOUR_AVEC_RECHERCHE_PAR_IDENTIFIANT + " -- " + CounterDAO.ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(03.)CAS D'ERREUR METIER SUIVANTE :
		//     LA LIGNE CONTENANT L'ENTITE COMPORTANT L'IDENTIFIANT RECHERCHE N'A PAS ETE TROUVEE:
		//     ->LANCER UNE EXCEPTION 'EntityNotFoundException'. 
		//////////////////////////////////////////////////////////////////////////////////
		if (hasBeenFound == false) {
			throw new EntityNotFoundException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_MISE_A_JOUR_AVEC_RECHERCHE_PAR_IDENTIFIANT + " -- " + CounterDAO.ERREUR_ENTITE_INTROUVABLE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(04.)CALCULER LA POSITION (DANS LE FICHIER) DU DEBUT DE LA LIGNE RECUPEREE.
		//     UTILISER POUR CELA LES ELEMENTS SUIVANTS :
		//     ->LA POSITION RECUPEREE PRECEDEMMENT. 
		//     ->LIGNE RECUPEREE PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		positionFound -= (ligneFound.length() + 1);
		
		//////////////////////////////////////////////////////////////////////////////////
		//(05.)SUPPRIMER (DANS LE FICHIER) LA LIGNE DECRITE CI-DESSOUS :
		//     ->DEBUT DE LA LIGNE    : LA POSITION CALCULEE PRECEDEMMENT.
		//     ->FIN DE LA LIGNE      : LE RETOUR CHARIOT.
		//////////////////////////////////////////////////////////////////////////////////
		this.deleteLineFromLineBegin(positionFound);
		
		//////////////////////////////////////////////////////////////////////////////////
		//(06.)RENVOYER L'OBJET 'Counter' TROUVE PRECEDEMMENT.
		//////////////////////////////////////////////////////////////////////////////////
		return counterFound;
	}
	
	
	/**
	 * <b>RECUPERER UN IDENTIFIANT UNIQUE</b>
	 * <br><b>(01.)Op�ration de recherche dans le fichier 'CSV':</b>
	 * <br>     ->Colonne � parcourir : La colonne 'nom'.
	 * <br>     ->Valeur � rechercher : La valeur fournie en param�tre.
	 * <br><b>(02.)Op�ration de mise � jour dans le fichier 'CSV':</b>
	 * <br>     ->Ligne � mettre � jour : La ligne trouv�e.
	 * <br>     ->Colonne � mettre � jour : La colonne 'valeur' (� incr�menter).
	 * 
	 * @param pNom La valeur � rechercher (Colonne � parcourir : La 2�me colonne dans le fichier 'CSV') 
	 * @return L'identifiant unique r�cup�r�.
	 * 
	 * @throws EntityNotFoundException Situation : La ligne n'a pas �t� trouv�e.
	 * @throws DAOException Situation : Erreur technique dans le DAO.
	 */
	public long getUniqueId(String pNom) throws EntityNotFoundException, DAOException {
		
		//////////////////////////////////////////////////////////////////////////////////
		//(01.)OUVRIR LE FICHIER 'counter.csv'
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)SUR LE FICHIER 'counter.csv', EFFECTUER LE TRAITEMENT NOMINAL SUIVANT :
		//////////////////////////////////////////////////////////////////////////////////
		//     (02.01.)PARCOURIR LE FICHIER LIGNE PAR LIGNE.
		//     (02.02.)SUR CHAQUE LIGNE DU FICHIER, EFFECTUER LA SEQUENCE D'OPERATIONS SUIVANTE :
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
		//             (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Counter'.
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES :
		//                        ->L'ATTRIBUT 'nom' DE L'OBJET 'Counter' RECUPERE.
		//                        ->LE PARAMETRE 'pNom' FOURNI EN ARGUMENT.
		//                        SI CES 2 ELEMENTS SONT NON IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.03.01.)METTRE LA CONDITION DE SORTIE DE BOUCLE A 'FAUX'.
		//////////////////////////////////////////////////////////////////////////////////
		//             (02.02.04.)CAS OU CES 2 ELEMENTS PRECEDENTS SONT IDENTIQUES :
		//                        SI CES 2 ELEMENTS SONT IDENTIQUES, EFFECTUER LES OPERATIONS SUIVANTES :
		//                        (02.02.04.01.)RECUPERER LA POSITION ACTUELLE DU CURSEUR (DANS LE FICHIER).
		//                        (02.02.04.02.)QUITTER LA BOUCLE DE PARCOURS DU FICHIER.
		//////////////////////////////////////////////////////////////////////////////////
		long positionFound = 0;
		String ligneFound = null;
		Counter counterFound = null;
		boolean hasBeenFound = false; 
		
		try(RandomAccessFile raf = new RandomAccessFile (new File (FILE), "r")) {
			
			raf.seek(0);
			
			do {
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.01.)RECUPERER LA LIGNE DANS UNE CHAINE DE CARACTERE.
				//////////////////////////////////////////////////////////////////////////////////
				String ligneCourante = raf.readLine();
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.02.)TRANSFORMER LA LIGNE EN UN OBJET 'Counter'.
				//////////////////////////////////////////////////////////////////////////////////
				if((ligneCourante == null) || (ligneCourante.isEmpty())) {
					hasBeenFound = false;
					break;
				}
				Counter counterCourant = this.transformLineToObject(ligneCourante);
				
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.03.)CAS OU LES 2 ELEMENTS SUIVANTS SONT NON IDENTIQUES : 
				//            ->L'ATTRIBUT 'nom' DE L'OBJET 'Counter' RECUPERE.
				//            ->LA VARIABLE 'pNom' FOURNIE EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				if ((counterCourant == null) || (counterCourant.getNom() == null)) {
					hasBeenFound = false;
					continue;
				}
				if (!counterCourant.getNom().equals(pNom)) {
					hasBeenFound = false;
					continue;
				}
				//////////////////////////////////////////////////////////////////////////////////
				// (02.02.04.)CAS OU LES 2 ELEMENTS SUIVANTS SONT IDENTIQUES : 
				//            ->L'ATTRIBUT 'nom' DE L'OBJET 'Counter' RECUPERE.
				//            ->LA VARIABLE 'pNom' FOURNIE EN ARGUMENT.
				//////////////////////////////////////////////////////////////////////////////////
				positionFound = raf.getFilePointer();
				ligneFound = ligneCourante;
				counterFound = counterCourant;
				hasBeenFound = true;
				break;
				
			} while (!hasBeenFound);
			
		} catch (FileNotFoundException fnfe) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_RECUPERATION_D_UN_IDENTIFIANT_UNIQUE + " -- " + CounterDAO.ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE);		
			
		} catch (IOException ioe) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_RECUPERATION_D_UN_IDENTIFIANT_UNIQUE + " -- " + CounterDAO.ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(03.)CAS D'ERREUR METIER SUIVANTE :
		//     LA LIGNE CONTENANT L'ENTITE COMPORTANT L'IDENTIFIANT RECHERCHE N'A PAS ETE TROUVEE:
		//     ->LANCER UNE EXCEPTION 'EntityNotFoundException'. 
		//////////////////////////////////////////////////////////////////////////////////
		if (hasBeenFound == false) {
			throw new EntityNotFoundException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_RECUPERATION_D_UN_IDENTIFIANT_UNIQUE + " -- " + CounterDAO.ERREUR_ENTITE_INTROUVABLE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(04.)CALCULER LA POSITION (DANS LE FICHIER) DU DEBUT DE LA LIGNE RECUPEREE.
		//     UTILISER POUR CELA LES ELEMENTS SUIVANTS :
		//     ->LA POSITION RECUPEREE PRECEDEMMENT. 
		//     ->LIGNE RECUPEREE PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		positionFound -= (ligneFound.length()); //TODO A TESTER !!!
		
		//////////////////////////////////////////////////////////////////////////////////
		//(05.)METTRE A JOUR L'OBJET 'Counter' RECUPERE PRECEDEMMENT.
		//     EFFECTUER CETTE MISE A JOUR DE LA MANIERE SUIVANTES :
		//     ->INCREMENTER L'ATTRIBUT 'valeur'. 
		//////////////////////////////////////////////////////////////////////////////////
		long valeurFound = counterFound.getValeur();
		counterFound.setValeur(valeurFound+1);
		
		//////////////////////////////////////////////////////////////////////////////////
		//(06.)METTRE A JOUR UNE LIGNE DU FICHIER EN UTILISANT LES PARAMETRES SUIVANTS:
		//     ->LA POSITION CALCULEE PRECEDEMMENT. 
		//     ->L'OBJET 'Counter' MIS A JOUR PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		this.updateAtPosition(positionFound, counterFound);
		
		//////////////////////////////////////////////////////////////////////////////////
		//(07.)RENVOYER L'ELEMENT SUIVANT :
		//     ->L'ATTRIBUT 'valeur' DE L'OBJET 'Counter' MIS A JOUR PRECEDEMMENT. 
		//////////////////////////////////////////////////////////////////////////////////
		return counterFound.getValeur();
	}
	
	@Override
	protected Counter transformLineToObject(String pLine) throws DAOException {
		
		///////////////////////////////////////////////////
		//(01.)DECOUPER LA LIGNE FOURNIE 
		///////////////////////////////////////////////////
		String[] proprietes = pLine.split(";");
		
		///////////////////////////////////////////////////
		//(02.)TRAITER LE CAS D'ERREUR : 
		//     PARMI LES 2 NOMBRES SUIVANTS, LE 1ER EST INFERIEUR AU 2EME. 
		//     ->LE NOMBRE D'ELEMENTS DANS LE TABLEAU 'proprietes' 
		//     ->LE NOMBRE D'ATTRIBUTS DANS L'ENTITE.
		///////////////////////////////////////////////////
		if ((proprietes == null) || (proprietes.length < CounterDAO.ENTITE_NOMBRE_D_ATTRIBUTS)) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_DECOUPAGE_D_UNE_LIGNE_DE_FICHIER + " -- " + CounterDAO.ERREUR_LIGNE_DE_FICHIER_FORMAT_INCORRECT);
		}
		///////////////////////////////////////////////////
		//(02.)CREER ET ALIMENTER LES PROPRIETES D'UN OBJET 'Counter'
		///////////////////////////////////////////////////
		long counterId = Long.parseUnsignedLong(proprietes[0]);
		String counterNom = proprietes[1];
		long counterValeur = Long.parseUnsignedLong(proprietes[2]);
		
		///////////////////////////////////////////////////
		//(03.)CREER ET ALIMENTER UN OBJET 'Counter'
		///////////////////////////////////////////////////
		Counter counter = new Counter(counterId, counterNom, counterValeur);
		
		///////////////////////////////////////////////////
		//(04.)RENVOYER L'OBJET 'Counter'
		///////////////////////////////////////////////////
		return counter;
	}
	
	@Override
	protected Counter updateAtPosition(long pPosition, Counter pT) throws DAOException {
		
		//////////////////////////////////////////////////////////////////////////////////
		//(01.)OUVRIR LE FICHIER 'counter.csv'
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)SUR LE FICHIER 'counter.csv', EFFECTUER LE TRAITEMENT NOMINAL SUIVANT :
		//////////////////////////////////////////////////////////////////////////////////
		//     (02.01.)POSITIONNER LE CURSEUR A LA POSITION FOURNIE EN ARGUMENT.
		//     (02.02.)ECRIRE UNE LIGNE DANS LE FICHIER CONFORMEMENT AUX EXIGENCES SUIVANTES:
		//             ->ECRIRE A LA POSITION ACTUELLE.
		//             ->ECRIRE EN UTILISANT LA CHAINE DE CARACTERE PRODUITE PAR L'OBJET 'Counter' FOURNI.
		//             ->ECRIRE UN RETOUR CHARIOT APRES LA FIN DE LA LIGNE.
		//////////////////////////////////////////////////////////////////////////////////
		try(RandomAccessFile raf = new RandomAccessFile (new File (FILE), "rw")) {
			
			//////////////////////////////////////////////////////////////////////////////////
			// (02.01.)POSITIONNER LE CURSEUR A LA POSITION FOURNIE EN ARGUMENT.
			//////////////////////////////////////////////////////////////////////////////////
			raf.seek(pPosition);
			
			//////////////////////////////////////////////////////////////////////////////////
			// (02.02.)ECRIRE UNE LIGNE DANS LE FICHIER CONFORMEMENT AUX EXIGENCES SUIVANTES:
			//         ->ECRIRE A LA POSITION ACTUELLE.
			//         ->ECRIRE EN UTILISANT LA CHAINE DE CARACTERE PRODUITE PAR L'OBJET 'Counter' FOURNI.
			//         ->ECRIRE UN RETOUR CHARIOT APRES LA FIN DE LA LIGNE.
			//////////////////////////////////////////////////////////////////////////////////
			raf.writeBytes(pT.toString());
			
		} catch (FileNotFoundException e) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_MODIFICATION_D_UNE_LIGNE_DE_FICHIER + " -- " + CounterDAO.ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE);		
			
		} catch (IOException e1) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_MODIFICATION_D_UNE_LIGNE_DE_FICHIER + " -- " + CounterDAO.ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(03.)RENVOYER L'OBJET 'Counter' MODIFIE PRECEDEMMENT.
		//////////////////////////////////////////////////////////////////////////////////
		return pT;
	}
	
	@Override
	protected Counter appendAtEOF(Counter pT) throws DAOException {
		
		//////////////////////////////////////////////////////////////////////////////////
		//(01.)OUVRIR LE FICHIER 'counter.csv'
		//////////////////////////////////////////////////////////////////////////////////
		//(02.)SUR LE FICHIER 'counter.csv', EFFECTUER LE TRAITEMENT NOMINAL SUIVANT :
		//////////////////////////////////////////////////////////////////////////////////
		//     (02.01.)POSITIONNER LE CURSEUR A LA POSITION FOURNIE EN ARGUMENT.
		//     (02.02.)ECRIRE UNE LIGNE DANS LE FICHIER CONFORMEMENT AUX EXIGENCES SUIVANTES:
		//             ->ECRIRE A LA POSITION ACTUELLE.
		//             ->ECRIRE EN UTILISANT LA CHAINE DE CARACTERE PRODUITE PAR L'OBJET 'Counter' FOURNI.
		//             ->ECRIRE UN RETOUR CHARIOT APRES LA FIN DE LA LIGNE.
		//////////////////////////////////////////////////////////////////////////////////
		try(RandomAccessFile raf = new RandomAccessFile (new File (FILE), "rw")) {
			
			//////////////////////////////////////////////////////////////////////////////////
			// (02.01.)POSITIONNER LE CURSEUR A LA POSITION 'FIN DE FICHIER'.
			//////////////////////////////////////////////////////////////////////////////////
			long rafLength = raf.length();
			raf.seek(rafLength);
			
			//////////////////////////////////////////////////////////////////////////////////
			// (02.02.)ECRIRE UNE LIGNE DANS LE FICHIER CONFORMEMENT AUX EXIGENCES SUIVANTES:
			//         ->ECRIRE A LA POSITION ACTUELLE.
			//         ->ECRIRE EN UTILISANT LA CHAINE DE CARACTERE PRODUITE PAR L'OBJET 'Counter' FOURNI.
			//         ->ECRIRE UN RETOUR CHARIOT APRES LA FIN DE LA LIGNE.
			//////////////////////////////////////////////////////////////////////////////////
			if (rafLength > 0) {
				raf.writeByte('\n');
			}
			raf.writeBytes(pT.toString());
			
		} catch (FileNotFoundException e) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_RAJOUT_D_UNE_LIGNE_DE_FICHIER + " -- " + CounterDAO.ERREUR_OUVERTURE_DU_FICHIER_ECHOUEEE);		
			
		} catch (IOException e1) {
			throw new DAOException(CounterDAO.ENTITE + " -- " + CounterDAO.FONCTIONNALITE_RAJOUT_D_UNE_LIGNE_DE_FICHIER + " -- " + CounterDAO.ERREUR_FERMETURE_DU_FICHIER_ECHOUEEE);
		}
		//////////////////////////////////////////////////////////////////////////////////
		//(03.)RENVOYER L'OBJET 'Counter' MODIFIE PRECEDEMMENT.
		//////////////////////////////////////////////////////////////////////////////////
		return pT;
	}
	
	@Override
	protected long deleteLineFromLineBegin(long pPosition) throws DAOException {
		
	}
}
