package persistence.exception;

/**
 * <b>EXCEPTION LANCEE LORS D'UNE OPERATION DE PERSISTANCE</b>
 * <br><b>DESCRIPTION : SITUATION DANS LAQUELLE UNE ERREUR 'TECHNIQUE' EST SURVENUE</b>
 * <br><b>INFORMATION : UNE ERREUR 'TECHNIQUE' EST UNE ERREUR 'NON-FONCTIONNELLE'</b>
 * 
 * @author Tcharou
 */
public class DAOException extends Exception {
	
	/**
	 * <b>DEFAULT SERIAL VERSION ID</b>
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * <b>CONSTRUCTEUR AVEC 1 ARGUMENT</b>
	 */
	public DAOException(String pMessage) {
		super(pMessage);
	}
}
