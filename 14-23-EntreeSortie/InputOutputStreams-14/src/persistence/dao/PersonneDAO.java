package persistence.dao;

import java.util.List;

import business.exception.EntityAlreadyExistsException;
import business.exception.EntityNotFoundException;
import persistence.entity.Personne;


/**
 * <b>INTERFACE DECLARANT LES OPERATIONS DE PERSISTANCE SUR L'ENTITE 'PERSONNE'</b>
 * <b>INTERFACE NON GENERIQUE : ELLE S'APPLIQUE A L'ENTITE CONCRETE 'PERSONNE'</b>
 * 
 * @author BOUHBEL     Azzedine, 
 * @author DA ROCHA    Manuel, 
 * @author DESCHAMPS   Francis, 
 * @author DORE        Emeryck, 
 * @author DURINGER    Gaspard, 
 * @author EL FATEOUI  Najim, 
 * @author FLAMAND     Kevin, 
 * @author GARNIER     Thomas, 
 * @author LAVIGNON    Baptiste, 
 * @author MARTIN      Guillaume, 
 * @author MASSE       Auriane, 
 * @author MOHAMED     Amin, 
 * @author PLOUCHARD   C�dric, 
 * @author RAJAOMAZAVA Dina, 
 * @author SALAME      Alexandre, 
 * @author SILOTIA     Gilles, 
 * @author VUILLAUME   Laureen, 
 */
public class PersonneDAO implements IDAO<Personne> {

	@Override
	public Personne create(Personne pT) throws EntityAlreadyExistsException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Personne findById(long pId) throws EntityNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Personne> findList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Personne update(Personne pT) throws EntityNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Personne delete(long pId) throws EntityNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
