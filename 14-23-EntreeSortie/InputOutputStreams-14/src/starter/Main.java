package starter;

import persistence.entity.Personne;
import ui.controller.PersonneController;

public class Main {

	public static void main(String[] args) {
		
		/////////////////////////////////////////////////
		//(01.)CREER UN OBJET DE TYPE 'PERSONNE-UI'
		/////////////////////////////////////////////////
		PersonneController personneUI = new PersonneController();
		
		/////////////////////////////////////////////////
		//(02.)CREER DES ENTITES DE TYPE 'PERSONNE'
		/////////////////////////////////////////////////
		Personne personne01 = new Personne("Dalgalian", "Tcharou"     , 49);
		Personne personne02 = new Personne("Pagan"    , "Jean-jacques", 43);
		Personne personne03 = new Personne("Bachri"   , "Amine"       , 49);
		
		/////////////////////////////////////////////////
		//(03.)ENREGISTRER LES ENTITES CREEES DANS L'INTERFACE UTILISATEUR
		/////////////////////////////////////////////////
		personneUI.enregistrer(personne01);
		personneUI.enregistrer(personne02);
		personneUI.enregistrer(personne03);
	}
}
