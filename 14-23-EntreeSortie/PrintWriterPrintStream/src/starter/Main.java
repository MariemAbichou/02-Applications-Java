package starter;

import persistence.PrintWriterPrintStream;

/**
 * <b>CLASSE PRINCIPALE DE L'APPLICATION :</b><br>
 * 
 * @author Tcharou
 */
public class Main {

	public static void main(String[] args) {
		
		PrintWriterPrintStream printWriterPrintStream = new PrintWriterPrintStream();
		printWriterPrintStream.execute();
	}
}
