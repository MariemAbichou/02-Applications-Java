package persistence;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;


/**
 * <b>INTERFACE DECLARANT LES OPERATIONS DE PERSISTANCE SUR L'ENTITE 'PERSONNE'</b>
 * <b>INTERFACE NON GENERIQUE : ELLE S'APPLIQUE A L'ENTITE CONCRETE 'PERSONNE'</b>
 * 
 * @author BOUHBEL     Azzedine, 
 * @author DA ROCHA    Manuel, 
 * @author DESCHAMPS   Francis, 
 * @author DORE        Emeryck, 
 * @author DURINGER    Gaspard, 
 * @author EL FATEOUI  Najim, 
 * @author FLAMAND     Kevin, 
 * @author GARNIER     Thomas, 
 * @author LAVIGNON    Baptiste, 
 * @author MARTIN      Guillaume, 
 * @author MASSE       Auriane, 
 * @author MOHAMED     Amin, 
 * @author PLOUCHARD   C�dric, 
 * @author RAJAOMAZAVA Dina, 
 * @author SALAME      Alexandre, 
 * @author SILOTIA     Gilles, 
 * @author VUILLAUME   Laureen, 
 */
public class PersonneDAO {

	
	private static final String PERSONNE_FILE = "data\\personne.csv";

	
	/**
	 * <b>CREER UNE LIGNE CORRESPONDANT A UNE ENTITE 'PERSONNE' DANS LES DONNEES PERSISTANTES</b>
	 * @param pPersonneLine La ligne correspondant � l'entit� 'PERSONNE' � cr�er.
	 * @return   La ligne correspondant � l'entit� 'PERSONNE' cr��e.
	 */
	public String create (String pPersonneLine) {

        //char[] chars = content.toCharArray();

        try (PrintWriter printWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(PERSONNE_FILE, true))))) {

            // Write the string and change line
            printWriter.println(pPersonneLine);

            // Format the output
            //printWriter.printf("%s\n",personneLineMichel);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return pPersonneLine;
	}
}
