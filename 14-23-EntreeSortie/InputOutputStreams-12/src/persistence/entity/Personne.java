package persistence.entity;

/**
 * <b>DEFINITION DE L'ENTITE 'PERSONNE'</b>
 * @author Tcharou
 *
 */
public class Personne {

	private long id;
	private String nom;
	private String prenom;
	private int age;
	
	/**
	 * <b>CONSTRUCTEUR SANS ARGUMENTS</b>
	 */
	public Personne() {}
	
	/**
	 * <b>CONSTRUCTEUR AVEC 3 ARGUMENTS</b>
	 */
	public Personne(String pNom, String pPrenom, int pAge) {
		this.nom    = pNom;
		this.prenom = pPrenom;
		this.age    = pAge;
	}

	/**
	 * <b>AFFICHAGE DU CONTENU DE L'OBJET</b>
	 */
	public String toString() {
		String result = this.nom + ";" + this.prenom + ";" + this.age;
		return result;
	}

	public long   getId    () { return this.id;     }
	public String getNom   () { return this.nom;    }
	public String getPrenom() { return this.prenom; }
	public int    getAge   () { return this.age;    }

	public void setId    (long   pId    ) { this.id     = pId;     }
	public void setNom   (String pNom   ) { this.nom    = pNom;    }
	public void setPrenom(String pPrenom) { this.prenom = pPrenom; }
	public void setAge   (int    pAge   ) { this.age    = pAge;    }
	
}
