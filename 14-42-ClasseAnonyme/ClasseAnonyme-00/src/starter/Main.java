package starter;

import service.Worker;
import user.User;

/**
 * <b>CLASSE D'ENTREE DE L'APPLICATION</b><br/>
 * @author 1603599
 *
 */
public class Main {

	/**
	 * <b>METHODE D'ENTREE DE L'APPLICATION</b><br/>
	 * @author 1603599
	 *
	 */
	public static void main(String[] args) {
		
		User user = new User();
		
		user.use();
	}
	
}
