package factory;

import model.Personne;

/**
 * <b>INTERFACE DE CREATION</b><br/>
 *    ->FABRIQUE D'OBJETS DE TYPE 'Personne'.<br/>
 * @author 1603599
 *
 */
public interface PersonneFactory extends Factory<Personne> {

	
	/**
	 * <b>METHODE DE CREATION</b><br/>
	 *    ->FABRIQUER UN OBJET DE TYPE 'Personne'.<br/>
	 *
	 * @author 1603599
	 * @param pNom
	 * @param pPrenom
	 */
	public Personne create(String pNom, String pPrenom);
}
