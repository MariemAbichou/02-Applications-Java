package starter;

import model.Personne;
import parser.Parser;

/**
 * <b>CLASSE D'ENTREE DE L'APPLICATION</b><br/>
 * @author 1603599
 *
 */
public class Main {

	/**
	 * <b>METHODE D'ENTREE DE L'APPLICATION</b><br/>
	 * @author 1603599
	 *
	 */
	public static void main(String[] args) {
		
		///////////////////////////////////////////////////////////////////////////////
		// (00.)CREER UNE CHAINE A PARSER.
		///////////////////////////////////////////////////////////////////////////////
		String chaineAParser = "DA ROCHA,Manuel";
		
		///////////////////////////////////////////////////////////////////////////////
		// (01.)CREER UN OBJET DE TYPE 'Parser<Personne>'.
		///////////////////////////////////////////////////////////////////////////////
		Parser<Personne> personneParser = new Parser<Personne>();
		
		///////////////////////////////////////////////////////////////////////////////
		// (02.)CREER UN OBJET 'Factory<Personne>'.
		///////////////////////////////////////////////////////////////////////////////
		// PAS BESOIN DE FAIRE CA !!!
		
		///////////////////////////////////////////////////////////////////////////////
		// (03.)SUR L'OBJET 'personneParser', DECLENCHER LA METHODE 'parse'.
		///////////////////////////////////////////////////////////////////////////////
		//Personne personneCree = personneParser.parse(chaineAParser, Personne::new);
		Personne personneCree = personneParser.parse(chaineAParser, (nom, prenom)->{return new Personne(nom, prenom);});
		
		///////////////////////////////////////////////////////////////////////////////
		// (04.)SUR L'OBJET 'personneCree', DECLENCHER LA METHODE 'parse'.
		///////////////////////////////////////////////////////////////////////////////
		System.out.println(personneCree.toString());
	}
}
