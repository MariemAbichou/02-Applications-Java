/**
 * 
 */
package starter;

import ui.PersonneUI;

/**
 * @author 1603599
 *
 */
public class Main {

	/**
	 * <b>POINT D'ENTREE DE L'APPLICATION</b>
	 * @param args
	 */
	public static void main(String[] args) {
		
		///////////////////////////////////////////////////////////////////////
		//(01.) CREER UN COMPOSANT D'INTERFACE (DU TYPE 'PERSONNE-UI')
		///////////////////////////////////////////////////////////////////////
		PersonneUI personneUser = new PersonneUI();
		
		///////////////////////////////////////////////////////////////////////
		//(02.) APPELER LA METHODE 'SEQUENCE-PRINCIPALE' DU COMPOSANT CREE
		///////////////////////////////////////////////////////////////////////
		personneUser.sequencePrincipale();
	
	}
	
}
