package ui;



import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import domain.Personne;



/**
 * <b>CLASSE QUI DEFINIT LE COMPOSANT D'INTERFACE UTILISATEUR RELATIF A LA PERSONNE</b>
 * @author Tcharou DALGALIAN
 */
public class PersonneUI {

	
	/**
	 * <b>L'OBJET "Scanner" RESPONSABLE DE LA LECTURE SUR LES FLUX D'ENTREE</b>
	 */
	private static Scanner scanner;
	

	/**
	 * <b>SAISIR DES PERSONNES, PUIS LES AFFICHER</b>
	 */
	public void sequencePrincipale(){

		///////////////////////////////////////////////////////////////////////
		//(00.)CREER UN OBJET "Scanner" (DE LECTURE DU FLUX D'ENTREE STANDARD)
		///////////////////////////////////////////////////////////////////////
		PersonneUI.scanner = new Scanner(System.in);
		
		///////////////////////////////////////////////////////////////////////
		//(01.)SAISIR DES PERSONNES (ET LES SAUVEGARDER DANS UNE MAP)
		///////////////////////////////////////////////////////////////////////
		Map<Integer, Personne> personnes = this.saisirMap();	

		///////////////////////////////////////////////////////////////////////
		//(02.)AFFICHER LES PERSONNES SAISIES PRECEDEMMENT
		///////////////////////////////////////////////////////////////////////
	    this.afficherMap(personnes);

		///////////////////////////////////////////////////////////////////////
		//(03.)FERMER L'OBJET "Scanner" (DE LECTURE DU FLUX D'ENTREE STANDARD)
		///////////////////////////////////////////////////////////////////////
		PersonneUI.scanner.close();
	}


	/**
	 * SAISIR DES PERSONNES (ET LES RANGER DANS UNE MAP)
	 * @return LA MAP DES PERSONNES A SAISIR
	 */
	private Map<Integer, Personne> saisirMap () {
		
		///////////////////////////////////////////////////////////////////////
		//(01.)CREER UNE MAP
		///////////////////////////////////////////////////////////////////////
		Map<Integer, Personne> personnes = new HashMap<Integer, Personne>();

		///////////////////////////////////////////////////////////////////////
		//(02.)BOUCLE DE SAISIE DE PERSONNES
		///////////////////////////////////////////////////////////////////////
		boolean continuer = true;
		int compteur = 0;
		
		do {
			///////////////////////////////////////////////////////////////////////
			//(02.01.)SAISIR UNE PERSONNE
			///////////////////////////////////////////////////////////////////////
			Personne personneSaisie = this.saisir();
			
			///////////////////////////////////////////////////////////////////////
			//(02.02.)INCREMENTER LE COMPTEUR DE PERSONNES
			///////////////////////////////////////////////////////////////////////
			compteur++;
			
			///////////////////////////////////////////////////////////////////////
			//(02.03.)INSERER LA PERSONNE SAISIE DANS LA MAP
			///////////////////////////////////////////////////////////////////////
			personnes.put(Integer.valueOf(compteur), personneSaisie);

			///////////////////////////////////////////////////////////////////////
			//(02.04.)CHOISIR DE CONTINUER LA SAISIE
			///////////////////////////////////////////////////////////////////////
			continuer = this.choisirContinuerSaisie();
			
		} while (continuer);
		
		///////////////////////////////////////////////////////////////////////
		//(03.)RENVOYER LA MAP
		///////////////////////////////////////////////////////////////////////
		return personnes;
	}
	
	
	/**
	 * AFFICHER LES PERSONNES DE LA MAP FOURNIE
	 * @param LA MAP DE PERSONNES A AFFICHER
	 */
	private void afficherMap (Map<Integer, Personne> pPersonnes) {
		
		///////////////////////////////////////////////////////////////////////
		//(01.)EXTRAIRE LE CONTENU DE LA MAP (SOUS LA FORME D'UNE LISTE DE COUPLES)
		///////////////////////////////////////////////////////////////////////
		Set<Entry<Integer, Personne>> couples = pPersonnes.entrySet();
		
		///////////////////////////////////////////////////////////////////////
		//(02.)PARCOURIR LA LISTE DES COUPLES EXTRAITE PRECEDEMMENT
		///////////////////////////////////////////////////////////////////////
	  	System.out.println(" TAILLE DE LA MAP         : " + pPersonnes.size());
	  	
    	System.out.println("+---------------------------------------------------+");
    	System.out.println("|          CONTENU DE LA MAP                        |");
    	System.out.println("+-------------+-------------------------------------+");
    	System.out.println("| INDEX :     | PERSONNE :                          |");
    	System.out.println("+-------------+-------------------------------------+");

    	for (Entry<Integer, Personne> couple : couples) {
    		
    		Integer cle = couple.getKey();
    		Personne personne = couple.getValue();
    		
        	System.out.println("| " + cle +    "        | " + personne.toString() + " |");
	    }
    	System.out.println("+-------------+-------------------------------------+");
    	return;
	}
	
	
	
	
	/**
	 * SAISIR UNE PERSONNE
	 * @return L'OBJET 'Personne' SAISI PAR L'UTILISATEUR.
	 */
	private Personne saisir() {

		//////////////////////////////////////////////////////////////////////////
		//(01.)AFFICHER UNE ANNONCE DE SAISIE
		//////////////////////////////////////////////////////////////////////////
		System.out.println("+------------------------------------------------------------+");
		System.out.println("| VEUILLEZ SAISIR LES INFORMATIONS RELATIVES A LA PERSONNE : |");
		System.out.println("+------------------------------------------------------------+");

		//////////////////////////////////////////////////////////////////////////
		//(02.)EFFECTUER LES SAISIES SUIVANTES :
		//     -->NOM    (de type String)
		//     -->PRENOM (de type String)
		//     -->AGE    (de type int   )
		//////////////////////////////////////////////////////////////////////////
		System.out.print("| Nom    : ");
		String nom = PersonneUI.scanner.nextLine();
		
		System.out.print("| Prenom : ");
		String prenom = PersonneUI.scanner.nextLine();
		
		System.out.print("| Age    : ");
		int age = PersonneUI.scanner.nextInt();
		PersonneUI.scanner.nextLine(); // VIDER LE FLUX D'ENTREE STANDARD (EXTRAIRE LE RETOUR CHARIOT RESIDUEL)
		
		System.out.println("+------------------------------------------------------------+");

		//////////////////////////////////////////////////////////////////////////
		//(03.)CREER UN OBJET 'PERSONNE' EN LUI FOURNISSANT LES DONNEES SAISIES PRECEDEMMENT :
		//     -->NOM   
		//     -->PRENOM
		//     -->AGE   
		//////////////////////////////////////////////////////////////////////////
		Personne personneSaisie = new Personne(nom, prenom, age);
		
		//////////////////////////////////////////////////////////////////////////
		//(04.)RENVOYER L'OBJET 'PERSONNE' CREE PRECEDEMMENT :
		//////////////////////////////////////////////////////////////////////////
		return personneSaisie;
	}
	
	
	/**
	 * CHOISIR DE CONTINUER LA SAISIE
	 * @return LE CHOIX SAISI PAR L'UTILISATEUR
	 */
	private boolean choisirContinuerSaisie() {
		
		/////////////////////////////////////////////////////////////////////////
		//(00.)DEFINIR LE RESULTAT A RENVOYER (= CHOIX DE CONTINUER LA SAISIE)
		/////////////////////////////////////////////////////////////////////////
		boolean choixContinuer = true;
		
		/////////////////////////////////////////////////////////////////////////
		//(01.)BOUCLE DE SAISIE DU CHOIX (DE CONTINUER LA SAISIE)
		/////////////////////////////////////////////////////////////////////////
		boolean choixIsValid = true;
		do {
			System.out.println("+------------------------------------------------------------+");
			System.out.println("| VOULEZ-VOUS SAISIR UNE PERSONNE (O / N) ?                  |");
			System.out.println("+------------------------------------------------------------+");

			System.out.print("| Votre choix : ");
			
			//PersonneUI.scanner.useDelimiter("");
			//String choixSaisi     = PersonneUI.scanner.next(); // EXTRAIRE 1 CARACTERE (ET 1 SEUL) DU FLUX D'ENTREE STANDARD.
			String choixSaisi     = PersonneUI.scanner.nextLine();
			
			char   choixSaisiChar = choixSaisi.charAt(0);
			int    choixSaisiInt  = (int)choixSaisiChar;
			
			System.out.println("| Choix saisi (de type 'String') : [" + choixSaisi     + "]");
			System.out.println("| Choix saisi (de type 'char'  ) : [" + choixSaisiChar + "]");
			System.out.println("| Choix saisi (de type 'int'   ) : [" + choixSaisiInt  + "]");
			
			System.out.println("+------------------------------------------------------------+");
			
			choixIsValid = (choixSaisiChar == 'O') || (choixSaisiChar == 'N');
			choixContinuer = (choixSaisiChar == 'O');

		} while (!choixIsValid);
		
		return choixContinuer;
	}
}
