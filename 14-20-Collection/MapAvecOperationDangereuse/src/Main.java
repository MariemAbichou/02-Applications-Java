import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		

		/////////////////////////////////////////////////////
		// (01.)CREATION D'UNE MAP 
		/////////////////////////////////////////////////////
		System.out.println("Je vais creer une map !!!");
		Map<String, String> personnes = new HashMap<String, String>();

		/////////////////////////////////////////////////////
		// (02.)AJOUT D'ELEMENTS DANS LA MAP
		/////////////////////////////////////////////////////
		System.out.println("Je vais remplir ma map !!!");
		personnes.put("A", "Jean-Pierre");
		personnes.put("B", "St�phane");
		personnes.put("C", "Philippe");
		
		/////////////////////////////////////////////////////
		// (03.)AFFICHAGE DU CONTENU DE LA MAP
		/////////////////////////////////////////////////////
		System.out.println("Je vais afficher le contenu de ma map !!!");
		System.out.println(personnes.get("A"));
		System.out.println(personnes.get("B"));
		System.out.println(personnes.get("C"));
		
		personnes.entrySet()

		/////////////////////////////////////////////////////
		// (04.)CALCUL ET AFFICHAGE DE LA TAILLE DE LA MAP
		/////////////////////////////////////////////////////
		System.out.println("Taille de la map: " + personnes.size());
		int positionHorsChamp = personnes.size();

		/////////////////////////////////////////////////////
		// (05.)VERIFICATION DE PRESENCE D'UNE CLE DANS LA MAP
		/////////////////////////////////////////////////////
		System.out.println("Verification de presence dans la map: ");
		System.out.println("Cle : D pr�sente�? -> R�ponse�: " + personnes.containsKey("D"));
		System.out.println("Cle : A pr�sente�? -> R�ponse�: " + personnes.containsKey("A"));

		System.out.println("Valeur : Jean-Pierre pr�sente�? -> R�ponse�: " + personnes.containsValue("Jean-Pierre"));

		/////////////////////////////////////////////////////
		// (05.)TENATIVE D'EXTRACTION D'ELEMENT NON-CONFORME A LA MAP
		//      -->Aucun �l�ment trouv� : ERREUR D'EXECUTION�!!!
		/////////////////////////////////////////////////////
		try {
			System.out.println("Je vais essayer un truc dingue avec ma map !!!");
			personnes.get(null);

		} catch(Exception e) {
			System.out.println("Erreur�:�" + e.getMessage());
			System.out.println("Et merde�!!! Finalement, ca n'a pas march� !!!");
		
		} finally {
			System.out.println("Je sais utiliser les maps en java !!!");
		}
	}

}
