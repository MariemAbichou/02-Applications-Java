import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		/////////////////////////////////////////////////////
		// (01.)CREATION D'UNE LISTE
		/////////////////////////////////////////////////////
		System.out.println("Je vais creer une liste !!!");
		List<String> noms = new ArrayList<String>();
		
		/////////////////////////////////////////////////////
		// (02.)AJOUT D'ELEMENTS DANS LA LISTE
		/////////////////////////////////////////////////////
		System.out.println("Je vais remplir ma liste !!!");
		noms.add("Jean-Pierre");
		noms.add("St�phane");
		noms.add("Philippe");
		
		/////////////////////////////////////////////////////
		// (03.)AFFICHAGE DU CONTENU DE LA LISTE
		/////////////////////////////////////////////////////
		System.out.println("Je vais afficher le contenu de ma liste !!!");
		System.out.println(noms.get(0));
		System.out.println(noms.get(1));
		System.out.println(noms.get(2));
		
		/////////////////////////////////////////////////////
		// (04.)CALCUL ET AFFICHAGE DE LA TAILLE DE LA LISTE
		/////////////////////////////////////////////////////
		System.out.println("Taille de la liste : " + noms.size());
		int positionHorsChamp = noms.size();
		
		/////////////////////////////////////////////////////
		// (05.)TENATIVE D'EXTRACTION D'ELEMENT AU-DELA DE LA LISTE
		// -->Aucun �l�ment trouv� : ERREUR D'EXECUTION !!!
		/////////////////////////////////////////////////////
		try {
			System.out.println("Je vais essayer un truc dingue avec ma liste !!!");
			// TODO A COMPLETER !!!
			noms.get('A');
			
		} catch (IndexOutOfBoundsException e) {
			System.out.println("Erreur : " + e.getMessage());
			System.out.println("Et merde !!! Finalement, ca n'a pas march� !!!");
			
		} finally {
			System.out.println("Je sais utiliser les listes en java !!!");
		}
	}
}
