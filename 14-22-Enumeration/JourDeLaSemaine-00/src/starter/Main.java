package starter;

import java.util.ArrayList;
import java.util.List;

import domain.JourDeLaSemaine;

public class Main {

	public static void main(String[] args) {

		//////////////////////////////////////////////////////////////////
		//(01.)CREER DES OBJETS DU TYPE 'JourDeLaSemaine'
		//////////////////////////////////////////////////////////////////
		JourDeLaSemaine jour01 = JourDeLaSemaine.LUNDI;
		JourDeLaSemaine jour02 = JourDeLaSemaine.MARDI;
		JourDeLaSemaine jour03 = JourDeLaSemaine.MERCREDI;
		JourDeLaSemaine jour04 = JourDeLaSemaine.JEUDI;
		JourDeLaSemaine jour05 = JourDeLaSemaine.VENDREDI;
		JourDeLaSemaine jour06 = JourDeLaSemaine.SAMEDI;
		JourDeLaSemaine jour07 = JourDeLaSemaine.DIMANCHE;
		
		//////////////////////////////////////////////////////////////////
		//(02.)CREER UNE LISTE D'OBJETS DU TYPE 'JourDeLaSemaine'
		//////////////////////////////////////////////////////////////////
		List<JourDeLaSemaine> jours = new ArrayList<JourDeLaSemaine>();
		
		//////////////////////////////////////////////////////////////////
		//(03.)ALIMENTER LA LISTE PRECEDENTE
		//////////////////////////////////////////////////////////////////
		jours.add(jour01);
		jours.add(jour02);
		jours.add(jour03);
		jours.add(jour04);
		jours.add(jour05);
		jours.add(jour06);
		jours.add(jour07);
		
		//////////////////////////////////////////////////////////////////
		//(04.)AFFICHER LE CONTENU DE LA LISTE PRECEDENTE
		//////////////////////////////////////////////////////////////////
		for (JourDeLaSemaine jour : jours) {
			System.out.println(jour.toString());
		}
		
		
	}

}
