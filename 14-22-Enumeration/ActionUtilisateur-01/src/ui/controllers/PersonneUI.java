/**
 * 
 */
package ui.controllers;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import ui.ActionUtilisateur;

/**
 * <b>CLASSE QUI DEFINIT LE COMPOSANT D'INTERFACE UTILISATEUR RELATIF A LA PERSONNE</b>
 * 
 * @author Tcharou DALGALIAN
 */
public class PersonneUI {

	/**
	 * <b>L'OBJET "Scanner" RESPONSABLE DE LA LECTURE SUR LES FLUX D'ENTREE</b>
	 */
	private static Scanner scanner;

	
	/**
	 * <b>BOUCLE PRINCIPALE DE L'INTERFACE UTILISATEUR</b>
	 */
	public void bouclePrincipale() {
		
		//////////////////////////////////////////////////////////////////////////
		//(01.) Afficher bienvenue
		//////////////////////////////////////////////////////////////////////////
		this.afficherBienvenue();
		
		///////////////////////////////////////////////////////////////////////
		//(02.)CREER UN OBJET "SCANNER" (DE LECTURE DU FLUX D'ENTREE STANDARD)
		///////////////////////////////////////////////////////////////////////
		PersonneUI.scanner = new Scanner(System.in);
		
		//////////////////////////////////////////////////////////////////////////
		//(03.) BOUCLE PRINCIPALE DE L'INTERFACE UTILISATEUR
		//- (03.01.) SAISIR UNE OPTION (DU MENU PRINCIPAL), PUIS L'EVALUER
		//- (03.02.) TRAITER L'OPTION CHOISIE (DU MENU PRINCIPAL)
		//- (03.03.) DEFINIR LA CONDITION DE MAINTIEN EN BOUCLE
		//////////////////////////////////////////////////////////////////////////
		ActionUtilisateur actionUtilisateur = null;
		boolean continuerSaisie = true;
		do {
			//////////////////////////////////////////////////////////////////////////
			//(03.01.) SAISIR UNE OPTION (DU MENU PRINCIPAL), PUIS L'EVALUER
			//////////////////////////////////////////////////////////////////////////
			actionUtilisateur = this.saisirEtEvaluerOptionMenuPrincipal();
			
			//////////////////////////////////////////////////////////////////////////
			//(03.02.) TRAITER L'OPTION CHOISIE (DU MENU PRINCIPAL)
			//////////////////////////////////////////////////////////////////////////
			actionUtilisateur = this.traiterOptionMenuPrincipal(actionUtilisateur);
			
			//////////////////////////////////////////////////////////////////////////
			//(03.03.) DEFINIR LA CONDITION DE MAINTIEN EN BOUCLE
			//////////////////////////////////////////////////////////////////////////
			continuerSaisie = (actionUtilisateur != ActionUtilisateur.QUITTER);
			
		} while (continuerSaisie);
		
		///////////////////////////////////////////////////////////////////////
		//(04.)FERMER L'OBJET "SCANNER" (DE LECTURE DU FLUX D'ENTREE STANDARD)
		///////////////////////////////////////////////////////////////////////
		PersonneUI.scanner.close();
	}
	
	/**
	 * <b>AFFICHER LE MESSAGE DE BIENVENUE DE L'APPLICATION</b>
	 */
	private void afficherBienvenue() {
		
		System.out.println("+---------------------------------------------------------+");
		System.out.println("|                                                         |");
		System.out.println("|                           GESTION DE PERSONNES          |");
		System.out.println("|                                                         |");
		System.out.println("+---------------------------------------------------------+");
	}
	
	/**
	 * <b>SAISIR UNE OPTION (DU MENU PRINCIPAL), PUIS L'EVALUER</b>
	 */
	private ActionUtilisateur saisirEtEvaluerOptionMenuPrincipal() {
		
		//////////////////////////////////////////////////////////////////////////
		//(01.) SAISIR ET EVALUER L'OPTION CHOISIE (DU MENU PRINCIPAL)
		//      - (01.01.)SAISIR UNE OPTION (DU MENU PRINCIPAL)
		//      - (01.02.)EVALUER L'OPTION CHOISIE (DU MENU PRINCIPAL)
		//      - (01.03.)DEFINIR LA CONDITION DE MAINTIEN EN BOUCLE
		//////////////////////////////////////////////////////////////////////////
		ActionUtilisateur actionUtilisateur = null;
		boolean continuerSaisie = true;
		do {
			//////////////////////////////////////////////////////////////////////////
			// (01.01.)SAISIR UNE OPTION (DU MENU PRINCIPAL)
			//////////////////////////////////////////////////////////////////////////
			int optionChoisie = this.saisirOptionMenuPrincipal();
			
			//////////////////////////////////////////////////////////////////////////
			// (01.02.)EVALUER L'OPTION CHOISIE (DU MENU PRINCIPAL)
			//////////////////////////////////////////////////////////////////////////
			actionUtilisateur = this.evaluerOptionChoisie(optionChoisie);
			
			//////////////////////////////////////////////////////////////////////////
			// (01.03.)DEFINIR LA CONDITION DE MAINTIEN EN BOUCLE
			//////////////////////////////////////////////////////////////////////////
			continuerSaisie = (actionUtilisateur == ActionUtilisateur.DEFAUT);
			
		} while (continuerSaisie);
		
		return actionUtilisateur;
	}
	
	/**
	 * <b>TRAITER L'OPTION CHOISIE (PAR L'UTILISATEUR)</b>
	 */
	private ActionUtilisateur traiterOptionMenuPrincipal(ActionUtilisateur pActionUtilisateur) {
		
		//////////////////////////////////////////////////////////////////////////
		//(01.) TRAITER L'OPTION CHOISIE (PAR L'UTILISATEUR)
		//////////////////////////////////////////////////////////////////////////
		switch(pActionUtilisateur) {
		
			case QUITTER :
				System.out.println("Option choisie : Quitter l'application");
				break;
			case CREER :
				System.out.println("Option choisie : Enregistrer une personne");
				break;
			case RECHERCHER_PAR_ID :
				System.out.println("Option choisie : Rechercher une personne (par son identifiant)");
				break;
			case RECHERCHER_LISTE :
				System.out.println("Option choisie : Afficher la liste de toutes les personnes");
				break;
			case MODIFIER :
				System.out.println("Option choisie : Modifier une personne");
				break;
			case SUPPRIMER :
				System.out.println("Option choisie : Supprimer une personne");
				break;
			default :
				System.out.println("Option choisie : choix non pr�vu");
				break;
		}
		//////////////////////////////////////////////////////////////////////////
		//(02.) RENVOYER L'OPTION CHOISIE (PAR L'UTILISATEUR)
		//////////////////////////////////////////////////////////////////////////
		return pActionUtilisateur;
	}
	
	/**
	 * <b>SAISIR UNE OPTION (DU MENU PRINCIPAL)</b>
	 */
	private int saisirOptionMenuPrincipal() {
		
		//////////////////////////////////////////////////////////////////////////
		//(01.) BOUCLE DE TRAITEMENT DES CAS D'ERREUR :
		// (01.01.) AFFICHER LA LISTE DES OPTIONS DU MENU PRINCIPAL
		// (01.02.) SAISIR UNE OPTION DU MENU PRINCIPAL 
		// (01.03.) DEFINIR LA CONDITION DE MAINTIEN EN BOUCLE
		//////////////////////////////////////////////////////////////////////////
		int optionChoisie = 0;
		boolean continuerSaisie = false;
		do {
			//////////////////////////////////////////////////////////////////////////
			// (01.01.) AFFICHER LA LISTE DES OPTIONS DU MENU PRINCIPAL
			//////////////////////////////////////////////////////////////////////////
			System.out.println("+---------------------------------------------------------+");
			System.out.println("|                        MENU PRINCIPAL                   |");
			System.out.println("+---------------------------------------------------------+");
			System.out.println("| 0 - Quitter l'application                               |");
			System.out.println("| 1 - Enregistrer une personne                            |");
			System.out.println("| 2 - Rechercher une personne (par son identifiant)       |");
			System.out.println("| 3 - Afficher la liste de toutes les personnes           |");
			System.out.println("| 4 - Modifier une personne                               |");
			System.out.println("| 5 - Supprimer une personne                              |");
			System.out.println("+---------------------------------------------------------+");
			
			//////////////////////////////////////////////////////////////////////////
			// (01.02.) SAISIR UNE OPTION DU MENU PRINCIPAL 
			//////////////////////////////////////////////////////////////////////////
			System.out.println("| Votre saisie : ");
			try {
				optionChoisie = PersonneUI.scanner.nextInt();
				continuerSaisie = false;
				
			} catch (InputMismatchException e) {
				System.out.println("ERREUR -- SAISIE DU CHOIX DE L'UTILISATEUR -- SAISIE D'UN TYPE INCORRECT");
				continuerSaisie = true;
				
			} catch (NoSuchElementException e) {
				System.out.println("ERREUR -- SAISIE DU CHOIX DE L'UTILISATEUR -- SAISIE MANQUANTE");
				continuerSaisie = true;
				
			} catch (IllegalStateException e) {
				System.out.println("ERREUR -- SAISIE DU CHOIX DE L'UTILISATEUR -- SAISIE AVEC UN FLUX DE LECTURE FERME");
				continuerSaisie = true;
				
			} finally {
				PersonneUI.scanner.nextLine();//VIDER FLUX D'ENTREE STANDARD (RETOUR CHARIOT RESIDUEL)
			}
			
		} while (continuerSaisie);
		
		return optionChoisie;
	}

	/**
	 * <b>EVALUER L'OPTION CHOISIE (DU MENU PRINCIPAL)</b>
	 */
	private ActionUtilisateur evaluerOptionChoisie(int pOptionChoisie) {
		
		//////////////////////////////////////////////////////////////////////////
		//(01.) CONVERTIR L'OPTION CHOISIE PAR L'UTILISATEUR EN UNE VALEUR AUTORISEE
		//////////////////////////////////////////////////////////////////////////
		ActionUtilisateur actionChoisie = null;
		switch (pOptionChoisie) {
		
			case 0 : 
				actionChoisie = ActionUtilisateur.QUITTER; 
				break;
			case 1 : 
				actionChoisie = ActionUtilisateur.CREER; 
				break;
			case 2 : 
				actionChoisie = ActionUtilisateur.RECHERCHER_PAR_ID; 
				break;
			case 3 : 
				actionChoisie = ActionUtilisateur.RECHERCHER_LISTE; 
				break;
			case 4 : 
				actionChoisie = ActionUtilisateur.MODIFIER; 
				break;
			case 5 : 
				actionChoisie = ActionUtilisateur.SUPPRIMER; 
				break;
		}
		return actionChoisie;
	}
}
