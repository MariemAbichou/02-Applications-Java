/**
 * 
 */
package ui;

/**
 * @author Tcharou
 *
 */
public enum ActionUtilisateur {
	
	///////////////////////////////////////////////////////////////////////////
	//(1.)OBJETS DIRECTEMENT CONSTRUITS
	///////////////////////////////////////////////////////////////////////////
	QUITTER           (0, "Quitter l'application"                        ),
	CREER             (1, "Enregistrer une personne"                     ),
	RECHERCHER_PAR_ID (2, "Rechercher une personne (par son identifiant)"),
	RECHERCHER_LISTE  (3, "Rechercher toutes les personnes"              ),
	MODIFIER          (4, "Modifier une personne"                        ),
	SUPPRIMER         (5, "Supprimer une personne"                       ),
	DEFAUT            (6, "Valeur non pr�vue"                            );
	
	/**
	 * <b>L'IDENTIFIANT DE L'ELEMENT ENUMERE</b>
	 */
	private int id;
	   
	/**
	 * <b>LA VALEUR DE L'ELEMENT ENUMERE</b>
	 */
	private String value;
	   
	/**
	 * <b>CONSTRUCTEUR AVEC ARGUMENTS</b>
	 */
	ActionUtilisateur(int pNumber, String pValue){
		this.id = pNumber;
		this.value = pValue;
	}
	   
	/**
	 * <b>RENVOYER LA VALEUR DE L'ELEMENT ENUMERE (SOUS FORME D'UNE CHAINE DE CARACTERES).</b>
	 * @return String La valeur de l'�l�ment �num�r� (sous forme d'une chaine de caract�res).
	 */
	@Override
	public String toString(){
		return "[" + this.id + "] -- [" + this.value + "]";
	}
}
