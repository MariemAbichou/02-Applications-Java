package service;

import java.util.Scanner;
import exceptions.ArithmeticException;

/**
 * <b>SERVICE METIER RESPONSABLE DES TRAITEMENTS SUIVANTS :</b>
 * <b>LE SERVICE DE DIVISION DE NOMBRES</b>
 * 
 * @author Tcharou
 *
 */
public class DivisionService {
	
	/**
	 * <b>ATTRIBUT DE TYPE 'Scanner' EXISTANT EN UN SEUL EXEMPLAIRE DANS TOUTE L'APPLICATION.</b>
	 */
	private static Scanner scanner;
	
	/**
	 * <b>METHODE RESPONSABLE DES ETAPES SUIVANTES :</b>
	 * <b>-->(01.)EFFECTUER LA SAISIE DE DEUX NOMBRES DECIMAUX</b>
	 * <b>-->(02.)EFFECTUER LA DIVISION ENTRE LES DEUX NOMBRES SAISIS</b>
	 * 
	 */
	public void diviserDecimaux() {
	
		///////////////////////////////////////////////////////
		//(01.)CREER L'OBJET DE TYPE 'Scanner' 
		///////////////////////////////////////////////////////
		DivisionService.scanner = new Scanner(System.in);
		
		///////////////////////////////////////////////////////
		//(02.)SAISIR LE DIVIDENDE
		///////////////////////////////////////////////////////
		System.out.println("Veuillez saisir le dividende :");
		float dividende = DivisionService.scanner.nextFloat();
		
		///////////////////////////////////////////////////////
		//(03.)SAISIR LE DIVISEUR
		///////////////////////////////////////////////////////
		System.out.println("Veuillez saisir le diviseur :");
		float diviseur = DivisionService.scanner.nextFloat();

		///////////////////////////////////////////////////////
		//(04.)EFFECTUER LA DIVISION ENTRE LE DIVIDENDE ET LE DIVISEUR SAISIS.
		///////////////////////////////////////////////////////
		float result = 0;
		try {
			result = this.diviser(dividende, diviseur);
			System.out.println("TRAITEMENT NOMINAL TERMINE");
			
		} catch (ArithmeticException e) {
			System.out.println("ERREUR : ArithmeticException -- " + e.getMessage());
			System.out.println("TRAITEMENT D'ERREUR TERMINE");	
			return;
			
		} finally {
			System.out.println("TRAITEMENT EFFECTUE DANS TOUS LES CAS DE FIGURE");	
		}
		///////////////////////////////////////////////////////
		//(05.)AFFICHER LE RESULTAT DE LA DIVISION EFFECTUEE.
		///////////////////////////////////////////////////////
		System.out.println("Le r�sultat de la division est :" + result);
		
		///////////////////////////////////////////////////////
		//(06.)FERMER L'OBJET DE TYPE 'Scanner' 
		///////////////////////////////////////////////////////
		DivisionService.scanner.close();
	}
	
	/**
	 * <b>METHODE RESPONSABLE DU TRAITEMENT SUIVANT :</b>
	 * <b>EFFECTUER LA DIVISION ENTRE DEUX NOMBRES DECIMAUX</b>
	 * 
	 * @param pDividende Le dividende
	 * @param pDiviseur Le diviseur
	 * @return Le r�sultat de la division
	 * @throws exceptions.ArithmeticException Lanc�e lorsque le diviseur est null. 
	 */
	private float diviser(float pDividende, float pDiviseur) throws ArithmeticException {
		
		///////////////////////////////////////////////////////
		//(01.)TRAITER LE CAS D'ERREUR : "DIVISEUR NULL"
		///////////////////////////////////////////////////////
		if (pDiviseur == 0) {
			throw new exceptions.ArithmeticException("Le diviseur est null ! Mon pauvre tu t'es lev� du pied gauche !");
		} 
		///////////////////////////////////////////////////////
		//(02.)TRATITER LE CAS NOMINAL : "DIVISEUR NON NULL"
		///////////////////////////////////////////////////////
		float result = pDividende/pDiviseur;
		
		return result;
	}
	

}
