package starter;

import service.DivisionService;

/**
 * <b>CLASSE PRINCIPALE DE L'APPLICATION</b>
 * 
 * @author Tcharou
 *
 */
public class Main {

	/**
	 * <b>POINT D'ENTREE DE L'APLICATION</b>
	 * 
	 * @args TABLEAU DE CHAINES DE CARACTERES FOURNI EN PARAMETRE 
	 */
	public static void main(String[] args) {
		
		///////////////////////////////////////////////////////
		//(01.)CREER LE SERVICE 'DIVISION-SERVICE'
		///////////////////////////////////////////////////////
		DivisionService divisionUI = new DivisionService();

		///////////////////////////////////////////////////////
		//(02.)LANCER LE SERVICE 'DIVISION-SERVICE'
		///////////////////////////////////////////////////////
		divisionUI.diviserDecimaux();
	}

}
